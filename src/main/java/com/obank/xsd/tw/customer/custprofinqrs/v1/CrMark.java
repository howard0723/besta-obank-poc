
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>CrMark complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CrMark"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LCrMark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCrMarkDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrMark", propOrder = {
    "lCrMark",
    "lCrMarkDate"
})
public class CrMark {

    @XmlElement(name = "LCrMark")
    protected String lCrMark;
    @XmlElement(name = "LCrMarkDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lCrMarkDate;

    /**
     * 取得 lCrMark 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCrMark() {
        return lCrMark;
    }

    /**
     * 設定 lCrMark 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCrMark(String value) {
        this.lCrMark = value;
    }

    /**
     * 取得 lCrMarkDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLCrMarkDate() {
        return lCrMarkDate;
    }

    /**
     * 設定 lCrMarkDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLCrMarkDate(XMLGregorianCalendar value) {
        this.lCrMarkDate = value;
    }

}
