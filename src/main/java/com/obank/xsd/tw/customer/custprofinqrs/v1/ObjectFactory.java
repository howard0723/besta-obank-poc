
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.obank.xsd.tw.customer.custprofinqrs.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceHeader_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "ServiceHeader");
    private final static QName _Signon_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "Signon");
    private final static QName _CustProfInqRs_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "CustProfInqRs");
    private final static QName _ServiceBody_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "ServiceBody");
    private final static QName _RtrnStatCode_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "RtrnStatCode");
    private final static QName _CustProfInfo_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "CustProfInfo");
    private final static QName _MailAddr_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "MailAddr");
    private final static QName _St_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "St");
    private final static QName _TownCnty_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "TownCnty");
    private final static QName _AddrZip_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "AddrZip");
    private final static QName _BirthAddr_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "BirthAddr");
    private final static QName _RelnInfo_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "RelnInfo");
    private final static QName _CertLic_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "CertLic");
    private final static QName _Empl_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "Empl");
    private final static QName _ResStat_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "ResStat");
    private final static QName _NameChngHist_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "NameChngHist");
    private final static QName _MKTMark_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "MKTMark");
    private final static QName _CrMark_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "CrMark");
    private final static QName _CtctSTat_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "CtctSTat");
    private final static QName _LSrc_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "LSrc");
    private final static QName _AddrRtrnStatCode_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "AddrRtrnStatCode");
    private final static QName _RsdntAddr_QNAME = new QName("http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", "RsdntAddr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.obank.xsd.tw.customer.custprofinqrs.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceHeaderType }
     * 
     */
    public ServiceHeaderType createServiceHeaderType() {
        return new ServiceHeaderType();
    }

    /**
     * Create an instance of {@link SignonType }
     * 
     */
    public SignonType createSignonType() {
        return new SignonType();
    }

    /**
     * Create an instance of {@link CustProfInqRs }
     * 
     */
    public CustProfInqRs createCustProfInqRs() {
        return new CustProfInqRs();
    }

    /**
     * Create an instance of {@link ServiceBody }
     * 
     */
    public ServiceBody createServiceBody() {
        return new ServiceBody();
    }

    /**
     * Create an instance of {@link RtrnStatCode }
     * 
     */
    public RtrnStatCode createRtrnStatCode() {
        return new RtrnStatCode();
    }

    /**
     * Create an instance of {@link CustProfInfo }
     * 
     */
    public CustProfInfo createCustProfInfo() {
        return new CustProfInfo();
    }

    /**
     * Create an instance of {@link MailAddr }
     * 
     */
    public MailAddr createMailAddr() {
        return new MailAddr();
    }

    /**
     * Create an instance of {@link St }
     * 
     */
    public St createSt() {
        return new St();
    }

    /**
     * Create an instance of {@link TownCnty }
     * 
     */
    public TownCnty createTownCnty() {
        return new TownCnty();
    }

    /**
     * Create an instance of {@link AddrZip }
     * 
     */
    public AddrZip createAddrZip() {
        return new AddrZip();
    }

    /**
     * Create an instance of {@link BirthAddr }
     * 
     */
    public BirthAddr createBirthAddr() {
        return new BirthAddr();
    }

    /**
     * Create an instance of {@link RelnInfo }
     * 
     */
    public RelnInfo createRelnInfo() {
        return new RelnInfo();
    }

    /**
     * Create an instance of {@link CertLic }
     * 
     */
    public CertLic createCertLic() {
        return new CertLic();
    }

    /**
     * Create an instance of {@link Empl }
     * 
     */
    public Empl createEmpl() {
        return new Empl();
    }

    /**
     * Create an instance of {@link ResStat }
     * 
     */
    public ResStat createResStat() {
        return new ResStat();
    }

    /**
     * Create an instance of {@link NameChngHist }
     * 
     */
    public NameChngHist createNameChngHist() {
        return new NameChngHist();
    }

    /**
     * Create an instance of {@link MKTMark }
     * 
     */
    public MKTMark createMKTMark() {
        return new MKTMark();
    }

    /**
     * Create an instance of {@link CrMark }
     * 
     */
    public CrMark createCrMark() {
        return new CrMark();
    }

    /**
     * Create an instance of {@link CtctSTat }
     * 
     */
    public CtctSTat createCtctSTat() {
        return new CtctSTat();
    }

    /**
     * Create an instance of {@link LSrc }
     * 
     */
    public LSrc createLSrc() {
        return new LSrc();
    }

    /**
     * Create an instance of {@link AddrRtrnStatCode }
     * 
     */
    public AddrRtrnStatCode createAddrRtrnStatCode() {
        return new AddrRtrnStatCode();
    }

    /**
     * Create an instance of {@link RsdntAddr }
     * 
     */
    public RsdntAddr createRsdntAddr() {
        return new RsdntAddr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceHeaderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "ServiceHeader")
    public JAXBElement<ServiceHeaderType> createServiceHeader(ServiceHeaderType value) {
        return new JAXBElement<ServiceHeaderType>(_ServiceHeader_QNAME, ServiceHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "Signon")
    public JAXBElement<SignonType> createSignon(SignonType value) {
        return new JAXBElement<SignonType>(_Signon_QNAME, SignonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustProfInqRs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "CustProfInqRs")
    public JAXBElement<CustProfInqRs> createCustProfInqRs(CustProfInqRs value) {
        return new JAXBElement<CustProfInqRs>(_CustProfInqRs_QNAME, CustProfInqRs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceBody }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "ServiceBody")
    public JAXBElement<ServiceBody> createServiceBody(ServiceBody value) {
        return new JAXBElement<ServiceBody>(_ServiceBody_QNAME, ServiceBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtrnStatCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "RtrnStatCode")
    public JAXBElement<RtrnStatCode> createRtrnStatCode(RtrnStatCode value) {
        return new JAXBElement<RtrnStatCode>(_RtrnStatCode_QNAME, RtrnStatCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustProfInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "CustProfInfo")
    public JAXBElement<CustProfInfo> createCustProfInfo(CustProfInfo value) {
        return new JAXBElement<CustProfInfo>(_CustProfInfo_QNAME, CustProfInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MailAddr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "MailAddr")
    public JAXBElement<MailAddr> createMailAddr(MailAddr value) {
        return new JAXBElement<MailAddr>(_MailAddr_QNAME, MailAddr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link St }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "St")
    public JAXBElement<St> createSt(St value) {
        return new JAXBElement<St>(_St_QNAME, St.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TownCnty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "TownCnty")
    public JAXBElement<TownCnty> createTownCnty(TownCnty value) {
        return new JAXBElement<TownCnty>(_TownCnty_QNAME, TownCnty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddrZip }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "AddrZip")
    public JAXBElement<AddrZip> createAddrZip(AddrZip value) {
        return new JAXBElement<AddrZip>(_AddrZip_QNAME, AddrZip.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BirthAddr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "BirthAddr")
    public JAXBElement<BirthAddr> createBirthAddr(BirthAddr value) {
        return new JAXBElement<BirthAddr>(_BirthAddr_QNAME, BirthAddr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelnInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "RelnInfo")
    public JAXBElement<RelnInfo> createRelnInfo(RelnInfo value) {
        return new JAXBElement<RelnInfo>(_RelnInfo_QNAME, RelnInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CertLic }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "CertLic")
    public JAXBElement<CertLic> createCertLic(CertLic value) {
        return new JAXBElement<CertLic>(_CertLic_QNAME, CertLic.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "Empl")
    public JAXBElement<Empl> createEmpl(Empl value) {
        return new JAXBElement<Empl>(_Empl_QNAME, Empl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResStat }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "ResStat")
    public JAXBElement<ResStat> createResStat(ResStat value) {
        return new JAXBElement<ResStat>(_ResStat_QNAME, ResStat.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NameChngHist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "NameChngHist")
    public JAXBElement<NameChngHist> createNameChngHist(NameChngHist value) {
        return new JAXBElement<NameChngHist>(_NameChngHist_QNAME, NameChngHist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MKTMark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "MKTMark")
    public JAXBElement<MKTMark> createMKTMark(MKTMark value) {
        return new JAXBElement<MKTMark>(_MKTMark_QNAME, MKTMark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrMark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "CrMark")
    public JAXBElement<CrMark> createCrMark(CrMark value) {
        return new JAXBElement<CrMark>(_CrMark_QNAME, CrMark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtctSTat }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "CtctSTat")
    public JAXBElement<CtctSTat> createCtctSTat(CtctSTat value) {
        return new JAXBElement<CtctSTat>(_CtctSTat_QNAME, CtctSTat.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LSrc }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "LSrc")
    public JAXBElement<LSrc> createLSrc(LSrc value) {
        return new JAXBElement<LSrc>(_LSrc_QNAME, LSrc.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddrRtrnStatCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "AddrRtrnStatCode")
    public JAXBElement<AddrRtrnStatCode> createAddrRtrnStatCode(AddrRtrnStatCode value) {
        return new JAXBElement<AddrRtrnStatCode>(_AddrRtrnStatCode_QNAME, AddrRtrnStatCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RsdntAddr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1", name = "RsdntAddr")
    public JAXBElement<RsdntAddr> createRsdntAddr(RsdntAddr value) {
        return new JAXBElement<RsdntAddr>(_RsdntAddr_QNAME, RsdntAddr.class, null, value);
    }

}
