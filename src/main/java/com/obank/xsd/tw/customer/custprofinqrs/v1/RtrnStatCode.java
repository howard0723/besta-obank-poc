
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RtrnStatCode complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="RtrnStatCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RtrnCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RtrnDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RtrnStatCode", propOrder = {
    "rtrnCode",
    "rtrnDesc"
})
public class RtrnStatCode {

    @XmlElement(name = "RtrnCode", required = true)
    protected String rtrnCode;
    @XmlElement(name = "RtrnDesc")
    protected String rtrnDesc;

    /**
     * 取得 rtrnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRtrnCode() {
        return rtrnCode;
    }

    /**
     * 設定 rtrnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRtrnCode(String value) {
        this.rtrnCode = value;
    }

    /**
     * 取得 rtrnDesc 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRtrnDesc() {
        return rtrnDesc;
    }

    /**
     * 設定 rtrnDesc 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRtrnDesc(String value) {
        this.rtrnDesc = value;
    }

}
