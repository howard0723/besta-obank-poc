
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ServiceBody complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="ServiceBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}RtrnStatCode" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}CustProfInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceBody", propOrder = {
    "rtrnStatCode",
    "custProfInfo"
})
public class ServiceBody {

    @XmlElement(name = "RtrnStatCode", required = true)
    protected List<RtrnStatCode> rtrnStatCode;
    @XmlElement(name = "CustProfInfo")
    protected CustProfInfo custProfInfo;

    /**
     * Gets the value of the rtrnStatCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rtrnStatCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRtrnStatCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RtrnStatCode }
     * 
     * 
     */
    public List<RtrnStatCode> getRtrnStatCode() {
        if (rtrnStatCode == null) {
            rtrnStatCode = new ArrayList<RtrnStatCode>();
        }
        return this.rtrnStatCode;
    }

    /**
     * 取得 custProfInfo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link CustProfInfo }
     *     
     */
    public CustProfInfo getCustProfInfo() {
        return custProfInfo;
    }

    /**
     * 設定 custProfInfo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link CustProfInfo }
     *     
     */
    public void setCustProfInfo(CustProfInfo value) {
        this.custProfInfo = value;
    }

}
