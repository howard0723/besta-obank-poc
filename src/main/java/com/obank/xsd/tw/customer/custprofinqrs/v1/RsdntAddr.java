
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RsdntAddr complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="RsdntAddr"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RsdntAddrId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustEngName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustChnName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}St" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}TownCnty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}AddrZip" minOccurs="0"/&gt;
 *         &lt;element name="Cntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PhnNo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RsdntAddr", propOrder = {
    "rsdntAddrId",
    "custEngName",
    "custChnName",
    "st",
    "townCnty",
    "addrZip",
    "cntry",
    "phnNo1"
})
public class RsdntAddr {

    @XmlElement(name = "RsdntAddrId", required = true)
    protected String rsdntAddrId;
    @XmlElement(name = "CustEngName", required = true)
    protected String custEngName;
    @XmlElement(name = "CustChnName", required = true)
    protected String custChnName;
    @XmlElement(name = "St")
    protected St st;
    @XmlElement(name = "TownCnty")
    protected TownCnty townCnty;
    @XmlElement(name = "AddrZip")
    protected AddrZip addrZip;
    @XmlElement(name = "Cntry")
    protected String cntry;
    @XmlElement(name = "PhnNo1")
    protected String phnNo1;

    /**
     * 取得 rsdntAddrId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsdntAddrId() {
        return rsdntAddrId;
    }

    /**
     * 設定 rsdntAddrId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsdntAddrId(String value) {
        this.rsdntAddrId = value;
    }

    /**
     * 取得 custEngName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEngName() {
        return custEngName;
    }

    /**
     * 設定 custEngName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEngName(String value) {
        this.custEngName = value;
    }

    /**
     * 取得 custChnName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustChnName() {
        return custChnName;
    }

    /**
     * 設定 custChnName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustChnName(String value) {
        this.custChnName = value;
    }

    /**
     * 取得 st 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link St }
     *     
     */
    public St getSt() {
        return st;
    }

    /**
     * 設定 st 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link St }
     *     
     */
    public void setSt(St value) {
        this.st = value;
    }

    /**
     * 取得 townCnty 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link TownCnty }
     *     
     */
    public TownCnty getTownCnty() {
        return townCnty;
    }

    /**
     * 設定 townCnty 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link TownCnty }
     *     
     */
    public void setTownCnty(TownCnty value) {
        this.townCnty = value;
    }

    /**
     * 取得 addrZip 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link AddrZip }
     *     
     */
    public AddrZip getAddrZip() {
        return addrZip;
    }

    /**
     * 設定 addrZip 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link AddrZip }
     *     
     */
    public void setAddrZip(AddrZip value) {
        this.addrZip = value;
    }

    /**
     * 取得 cntry 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCntry() {
        return cntry;
    }

    /**
     * 設定 cntry 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCntry(String value) {
        this.cntry = value;
    }

    /**
     * 取得 phnNo1 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhnNo1() {
        return phnNo1;
    }

    /**
     * 設定 phnNo1 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhnNo1(String value) {
        this.phnNo1 = value;
    }

}
