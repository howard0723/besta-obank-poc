
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>TownCnty complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="TownCnty"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GBTownCnty" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TWTownCnty" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TownCnty", propOrder = {
    "gbTownCnty",
    "twTownCnty"
})
public class TownCnty {

    @XmlElement(name = "GBTownCnty", required = true)
    protected String gbTownCnty;
    @XmlElement(name = "TWTownCnty", required = true)
    protected String twTownCnty;

    /**
     * 取得 gbTownCnty 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGBTownCnty() {
        return gbTownCnty;
    }

    /**
     * 設定 gbTownCnty 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGBTownCnty(String value) {
        this.gbTownCnty = value;
    }

    /**
     * 取得 twTownCnty 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTWTownCnty() {
        return twTownCnty;
    }

    /**
     * 設定 twTownCnty 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTWTownCnty(String value) {
        this.twTownCnty = value;
    }

}
