
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Empl complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="Empl"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EmplStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CoName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CoAddr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CoBuss" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmplDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Slry" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Empl", propOrder = {
    "emplStat",
    "coName",
    "coAddr",
    "coBuss",
    "emplDate",
    "slry"
})
public class Empl {

    @XmlElement(name = "EmplStat")
    protected String emplStat;
    @XmlElement(name = "CoName")
    protected String coName;
    @XmlElement(name = "CoAddr")
    protected List<String> coAddr;
    @XmlElement(name = "CoBuss")
    protected String coBuss;
    @XmlElement(name = "EmplDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar emplDate;
    @XmlElement(name = "Slry")
    protected BigDecimal slry;

    /**
     * 取得 emplStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmplStat() {
        return emplStat;
    }

    /**
     * 設定 emplStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmplStat(String value) {
        this.emplStat = value;
    }

    /**
     * 取得 coName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoName() {
        return coName;
    }

    /**
     * 設定 coName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoName(String value) {
        this.coName = value;
    }

    /**
     * Gets the value of the coAddr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coAddr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoAddr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCoAddr() {
        if (coAddr == null) {
            coAddr = new ArrayList<String>();
        }
        return this.coAddr;
    }

    /**
     * 取得 coBuss 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoBuss() {
        return coBuss;
    }

    /**
     * 設定 coBuss 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoBuss(String value) {
        this.coBuss = value;
    }

    /**
     * 取得 emplDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEmplDate() {
        return emplDate;
    }

    /**
     * 設定 emplDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEmplDate(XMLGregorianCalendar value) {
        this.emplDate = value;
    }

    /**
     * 取得 slry 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSlry() {
        return slry;
    }

    /**
     * 設定 slry 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSlry(BigDecimal value) {
        this.slry = value;
    }

}
