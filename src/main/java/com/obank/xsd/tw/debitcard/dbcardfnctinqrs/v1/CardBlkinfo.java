
package com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CardBlkinfo complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CardBlkinfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BlkSrcId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BlkCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BlkDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BlkTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardBlkinfo", propOrder = {
    "blkSrcId",
    "blkCode",
    "blkDate",
    "blkTime"
})
public class CardBlkinfo {

    @XmlElement(name = "BlkSrcId", required = true)
    protected String blkSrcId;
    @XmlElement(name = "BlkCode", required = true)
    protected String blkCode;
    @XmlElement(name = "BlkDate", required = true)
    protected String blkDate;
    @XmlElement(name = "BlkTime", required = true)
    protected String blkTime;

    /**
     * 取得 blkSrcId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlkSrcId() {
        return blkSrcId;
    }

    /**
     * 設定 blkSrcId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlkSrcId(String value) {
        this.blkSrcId = value;
    }

    /**
     * 取得 blkCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlkCode() {
        return blkCode;
    }

    /**
     * 設定 blkCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlkCode(String value) {
        this.blkCode = value;
    }

    /**
     * 取得 blkDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlkDate() {
        return blkDate;
    }

    /**
     * 設定 blkDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlkDate(String value) {
        this.blkDate = value;
    }

    /**
     * 取得 blkTime 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlkTime() {
        return blkTime;
    }

    /**
     * 設定 blkTime 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlkTime(String value) {
        this.blkTime = value;
    }

}
