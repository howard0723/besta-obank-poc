
package com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ControlRec complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="ControlRec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CurPage" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="NoOfCurPage" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="TtlPageNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="NoOfTxns" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlRec", propOrder = {
    "curPage",
    "noOfCurPage",
    "ttlPageNo",
    "noOfTxns"
})
public class ControlRec {

    @XmlElement(name = "CurPage", required = true)
    protected BigInteger curPage;
    @XmlElement(name = "NoOfCurPage", required = true)
    protected BigInteger noOfCurPage;
    @XmlElement(name = "TtlPageNo", required = true)
    protected BigInteger ttlPageNo;
    @XmlElement(name = "NoOfTxns", required = true)
    protected BigInteger noOfTxns;

    /**
     * 取得 curPage 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurPage() {
        return curPage;
    }

    /**
     * 設定 curPage 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurPage(BigInteger value) {
        this.curPage = value;
    }

    /**
     * 取得 noOfCurPage 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfCurPage() {
        return noOfCurPage;
    }

    /**
     * 設定 noOfCurPage 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfCurPage(BigInteger value) {
        this.noOfCurPage = value;
    }

    /**
     * 取得 ttlPageNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTtlPageNo() {
        return ttlPageNo;
    }

    /**
     * 設定 ttlPageNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTtlPageNo(BigInteger value) {
        this.ttlPageNo = value;
    }

    /**
     * 取得 noOfTxns 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfTxns() {
        return noOfTxns;
    }

    /**
     * 設定 noOfTxns 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfTxns(BigInteger value) {
        this.noOfTxns = value;
    }

}
