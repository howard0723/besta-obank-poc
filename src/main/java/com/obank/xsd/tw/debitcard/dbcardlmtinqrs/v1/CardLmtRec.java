
package com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CardLmtRec complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CardLmtRec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DbAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustPermId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardKind" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DayLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="MnthLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TempLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="LmtCcy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StartTempLmtDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EndTempLmtDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UpdDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UpdTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UpdtUserId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SrcCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AccumDayLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="AccumMnthLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="AvailDayLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="AvailMnthLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="AutoLoadLmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="TmpDayLmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="StartTmpDayLmtDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndTmpDayLmtDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardLmtRec", propOrder = {
    "cardNo",
    "dbAcctNo",
    "custPermId",
    "cardKind",
    "dayLmt",
    "mnthLmt",
    "tempLmt",
    "lmtCcy",
    "startTempLmtDate",
    "endTempLmtDate",
    "updDate",
    "updTime",
    "updtUserId",
    "srcCode",
    "accumDayLmt",
    "accumMnthLmt",
    "availDayLmt",
    "availMnthLmt",
    "autoLoadLmt",
    "tmpDayLmt",
    "startTmpDayLmtDate",
    "endTmpDayLmtDate"
})
public class CardLmtRec {

    @XmlElement(name = "CardNo", required = true)
    protected String cardNo;
    @XmlElement(name = "DbAcctNo", required = true)
    protected String dbAcctNo;
    @XmlElement(name = "CustPermId", required = true)
    protected String custPermId;
    @XmlElement(name = "CardKind", required = true)
    protected String cardKind;
    @XmlElement(name = "DayLmt", required = true)
    protected BigDecimal dayLmt;
    @XmlElement(name = "MnthLmt", required = true)
    protected BigDecimal mnthLmt;
    @XmlElement(name = "TempLmt", required = true)
    protected BigDecimal tempLmt;
    @XmlElement(name = "LmtCcy", required = true)
    protected String lmtCcy;
    @XmlElement(name = "StartTempLmtDate", required = true)
    protected String startTempLmtDate;
    @XmlElement(name = "EndTempLmtDate", required = true)
    protected String endTempLmtDate;
    @XmlElement(name = "UpdDate", required = true)
    protected String updDate;
    @XmlElement(name = "UpdTime", required = true)
    protected String updTime;
    @XmlElement(name = "UpdtUserId", required = true)
    protected String updtUserId;
    @XmlElement(name = "SrcCode", required = true)
    protected String srcCode;
    @XmlElement(name = "AccumDayLmt", required = true)
    protected BigDecimal accumDayLmt;
    @XmlElement(name = "AccumMnthLmt", required = true)
    protected BigDecimal accumMnthLmt;
    @XmlElement(name = "AvailDayLmt", required = true)
    protected BigDecimal availDayLmt;
    @XmlElement(name = "AvailMnthLmt", required = true)
    protected BigDecimal availMnthLmt;
    @XmlElement(name = "AutoLoadLmt")
    protected BigDecimal autoLoadLmt;
    @XmlElement(name = "TmpDayLmt")
    protected BigDecimal tmpDayLmt;
    @XmlElement(name = "StartTmpDayLmtDate")
    protected String startTmpDayLmtDate;
    @XmlElement(name = "EndTmpDayLmtDate")
    protected String endTmpDayLmtDate;

    /**
     * 取得 cardNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * 設定 cardNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * 取得 dbAcctNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDbAcctNo() {
        return dbAcctNo;
    }

    /**
     * 設定 dbAcctNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDbAcctNo(String value) {
        this.dbAcctNo = value;
    }

    /**
     * 取得 custPermId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPermId() {
        return custPermId;
    }

    /**
     * 設定 custPermId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPermId(String value) {
        this.custPermId = value;
    }

    /**
     * 取得 cardKind 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardKind() {
        return cardKind;
    }

    /**
     * 設定 cardKind 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardKind(String value) {
        this.cardKind = value;
    }

    /**
     * 取得 dayLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDayLmt() {
        return dayLmt;
    }

    /**
     * 設定 dayLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDayLmt(BigDecimal value) {
        this.dayLmt = value;
    }

    /**
     * 取得 mnthLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMnthLmt() {
        return mnthLmt;
    }

    /**
     * 設定 mnthLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMnthLmt(BigDecimal value) {
        this.mnthLmt = value;
    }

    /**
     * 取得 tempLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTempLmt() {
        return tempLmt;
    }

    /**
     * 設定 tempLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTempLmt(BigDecimal value) {
        this.tempLmt = value;
    }

    /**
     * 取得 lmtCcy 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLmtCcy() {
        return lmtCcy;
    }

    /**
     * 設定 lmtCcy 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLmtCcy(String value) {
        this.lmtCcy = value;
    }

    /**
     * 取得 startTempLmtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartTempLmtDate() {
        return startTempLmtDate;
    }

    /**
     * 設定 startTempLmtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTempLmtDate(String value) {
        this.startTempLmtDate = value;
    }

    /**
     * 取得 endTempLmtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndTempLmtDate() {
        return endTempLmtDate;
    }

    /**
     * 設定 endTempLmtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTempLmtDate(String value) {
        this.endTempLmtDate = value;
    }

    /**
     * 取得 updDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdDate() {
        return updDate;
    }

    /**
     * 設定 updDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdDate(String value) {
        this.updDate = value;
    }

    /**
     * 取得 updTime 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdTime() {
        return updTime;
    }

    /**
     * 設定 updTime 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdTime(String value) {
        this.updTime = value;
    }

    /**
     * 取得 updtUserId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdtUserId() {
        return updtUserId;
    }

    /**
     * 設定 updtUserId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdtUserId(String value) {
        this.updtUserId = value;
    }

    /**
     * 取得 srcCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcCode() {
        return srcCode;
    }

    /**
     * 設定 srcCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcCode(String value) {
        this.srcCode = value;
    }

    /**
     * 取得 accumDayLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAccumDayLmt() {
        return accumDayLmt;
    }

    /**
     * 設定 accumDayLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAccumDayLmt(BigDecimal value) {
        this.accumDayLmt = value;
    }

    /**
     * 取得 accumMnthLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAccumMnthLmt() {
        return accumMnthLmt;
    }

    /**
     * 設定 accumMnthLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAccumMnthLmt(BigDecimal value) {
        this.accumMnthLmt = value;
    }

    /**
     * 取得 availDayLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailDayLmt() {
        return availDayLmt;
    }

    /**
     * 設定 availDayLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailDayLmt(BigDecimal value) {
        this.availDayLmt = value;
    }

    /**
     * 取得 availMnthLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailMnthLmt() {
        return availMnthLmt;
    }

    /**
     * 設定 availMnthLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailMnthLmt(BigDecimal value) {
        this.availMnthLmt = value;
    }

    /**
     * 取得 autoLoadLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAutoLoadLmt() {
        return autoLoadLmt;
    }

    /**
     * 設定 autoLoadLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAutoLoadLmt(BigDecimal value) {
        this.autoLoadLmt = value;
    }

    /**
     * 取得 tmpDayLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTmpDayLmt() {
        return tmpDayLmt;
    }

    /**
     * 設定 tmpDayLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTmpDayLmt(BigDecimal value) {
        this.tmpDayLmt = value;
    }

    /**
     * 取得 startTmpDayLmtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartTmpDayLmtDate() {
        return startTmpDayLmtDate;
    }

    /**
     * 設定 startTmpDayLmtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTmpDayLmtDate(String value) {
        this.startTmpDayLmtDate = value;
    }

    /**
     * 取得 endTmpDayLmtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndTmpDayLmtDate() {
        return endTmpDayLmtDate;
    }

    /**
     * 設定 endTmpDayLmtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTmpDayLmtDate(String value) {
        this.endTmpDayLmtDate = value;
    }

}
