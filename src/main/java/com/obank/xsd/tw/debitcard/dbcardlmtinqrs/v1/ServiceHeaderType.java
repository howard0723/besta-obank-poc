
package com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ServiceHeaderType complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="ServiceHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TxnId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TxnNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NoOfTxns" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ChkSum" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceHeaderType", propOrder = {
    "txnId",
    "txnNo",
    "noOfTxns",
    "chkSum"
})
public class ServiceHeaderType {

    @XmlElement(name = "TxnId", required = true)
    protected String txnId;
    @XmlElement(name = "TxnNo", required = true)
    protected String txnNo;
    @XmlElement(name = "NoOfTxns")
    protected BigDecimal noOfTxns;
    @XmlElement(name = "ChkSum")
    protected BigDecimal chkSum;

    /**
     * 取得 txnId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * 設定 txnId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxnId(String value) {
        this.txnId = value;
    }

    /**
     * 取得 txnNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxnNo() {
        return txnNo;
    }

    /**
     * 設定 txnNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxnNo(String value) {
        this.txnNo = value;
    }

    /**
     * 取得 noOfTxns 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNoOfTxns() {
        return noOfTxns;
    }

    /**
     * 設定 noOfTxns 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNoOfTxns(BigDecimal value) {
        this.noOfTxns = value;
    }

    /**
     * 取得 chkSum 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChkSum() {
        return chkSum;
    }

    /**
     * 設定 chkSum 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChkSum(BigDecimal value) {
        this.chkSum = value;
    }

}
