
package com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CardBlkRec complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CardBlkRec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/DebitCard/DbCardFnctInqRs/v1}CardBlkinfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustPermId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardAtt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RelnCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ExpYearMnth" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CntlrId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardBlkRec", propOrder = {
    "cardBlkinfo",
    "cardNo",
    "cardName",
    "custPermId",
    "cardType",
    "cardAtt",
    "relnCode",
    "expYearMnth",
    "cntlrId",
    "memo"
})
public class CardBlkRec {

    @XmlElement(name = "CardBlkinfo")
    protected List<CardBlkinfo> cardBlkinfo;
    @XmlElement(name = "CardNo", required = true)
    protected String cardNo;
    @XmlElement(name = "CardName", required = true)
    protected String cardName;
    @XmlElement(name = "CustPermId", required = true)
    protected String custPermId;
    @XmlElement(name = "CardType", required = true)
    protected String cardType;
    @XmlElement(name = "CardAtt", required = true)
    protected String cardAtt;
    @XmlElement(name = "RelnCode", required = true)
    protected String relnCode;
    @XmlElement(name = "ExpYearMnth", required = true)
    protected String expYearMnth;
    @XmlElement(name = "CntlrId", required = true)
    protected String cntlrId;
    @XmlElement(name = "Memo", required = true)
    protected String memo;

    /**
     * Gets the value of the cardBlkinfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cardBlkinfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCardBlkinfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CardBlkinfo }
     * 
     * 
     */
    public List<CardBlkinfo> getCardBlkinfo() {
        if (cardBlkinfo == null) {
            cardBlkinfo = new ArrayList<CardBlkinfo>();
        }
        return this.cardBlkinfo;
    }

    /**
     * 取得 cardNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * 設定 cardNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * 取得 cardName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * 設定 cardName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardName(String value) {
        this.cardName = value;
    }

    /**
     * 取得 custPermId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPermId() {
        return custPermId;
    }

    /**
     * 設定 custPermId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPermId(String value) {
        this.custPermId = value;
    }

    /**
     * 取得 cardType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * 設定 cardType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * 取得 cardAtt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAtt() {
        return cardAtt;
    }

    /**
     * 設定 cardAtt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAtt(String value) {
        this.cardAtt = value;
    }

    /**
     * 取得 relnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelnCode() {
        return relnCode;
    }

    /**
     * 設定 relnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelnCode(String value) {
        this.relnCode = value;
    }

    /**
     * 取得 expYearMnth 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpYearMnth() {
        return expYearMnth;
    }

    /**
     * 設定 expYearMnth 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpYearMnth(String value) {
        this.expYearMnth = value;
    }

    /**
     * 取得 cntlrId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCntlrId() {
        return cntlrId;
    }

    /**
     * 設定 cntlrId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCntlrId(String value) {
        this.cntlrId = value;
    }

    /**
     * 取得 memo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * 設定 memo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

}
