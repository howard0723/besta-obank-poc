
package com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceHeader_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "ServiceHeader");
    private final static QName _Signon_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "Signon");
    private final static QName _DbCardLmtInqRs_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "DbCardLmtInqRs");
    private final static QName _ServiceBody_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "ServiceBody");
    private final static QName _CardLmtRec_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "CardLmtRec");
    private final static QName _ControlRec_QNAME = new QName("http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", "ControlRec");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceHeaderType }
     * 
     */
    public ServiceHeaderType createServiceHeaderType() {
        return new ServiceHeaderType();
    }

    /**
     * Create an instance of {@link SignonType }
     * 
     */
    public SignonType createSignonType() {
        return new SignonType();
    }

    /**
     * Create an instance of {@link DbCardLmtInqRs }
     * 
     */
    public DbCardLmtInqRs createDbCardLmtInqRs() {
        return new DbCardLmtInqRs();
    }

    /**
     * Create an instance of {@link ServiceBody }
     * 
     */
    public ServiceBody createServiceBody() {
        return new ServiceBody();
    }

    /**
     * Create an instance of {@link CardLmtRec }
     * 
     */
    public CardLmtRec createCardLmtRec() {
        return new CardLmtRec();
    }

    /**
     * Create an instance of {@link ControlRec }
     * 
     */
    public ControlRec createControlRec() {
        return new ControlRec();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceHeaderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "ServiceHeader")
    public JAXBElement<ServiceHeaderType> createServiceHeader(ServiceHeaderType value) {
        return new JAXBElement<ServiceHeaderType>(_ServiceHeader_QNAME, ServiceHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "Signon")
    public JAXBElement<SignonType> createSignon(SignonType value) {
        return new JAXBElement<SignonType>(_Signon_QNAME, SignonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DbCardLmtInqRs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "DbCardLmtInqRs")
    public JAXBElement<DbCardLmtInqRs> createDbCardLmtInqRs(DbCardLmtInqRs value) {
        return new JAXBElement<DbCardLmtInqRs>(_DbCardLmtInqRs_QNAME, DbCardLmtInqRs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceBody }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "ServiceBody")
    public JAXBElement<ServiceBody> createServiceBody(ServiceBody value) {
        return new JAXBElement<ServiceBody>(_ServiceBody_QNAME, ServiceBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardLmtRec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "CardLmtRec")
    public JAXBElement<CardLmtRec> createCardLmtRec(CardLmtRec value) {
        return new JAXBElement<CardLmtRec>(_CardLmtRec_QNAME, CardLmtRec.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ControlRec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/DebitCard/DbCardLmtInqRs/v1", name = "ControlRec")
    public JAXBElement<ControlRec> createControlRec(ControlRec value) {
        return new JAXBElement<ControlRec>(_ControlRec_QNAME, ControlRec.class, null, value);
    }

}
