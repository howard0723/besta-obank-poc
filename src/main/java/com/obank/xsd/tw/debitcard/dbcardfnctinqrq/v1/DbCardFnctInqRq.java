
package com.obank.xsd.tw.debitcard.dbcardfnctinqrq.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>DbCardFnctInqRq complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="DbCardFnctInqRq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceHeader" type="{http://www.obank.com/xsd/TW/DebitCard/DbCardFnctInqRq/v1}ServiceHeaderType"/&gt;
 *         &lt;element name="Signon" type="{http://www.obank.com/xsd/TW/DebitCard/DbCardFnctInqRq/v1}SignonType"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/DebitCard/DbCardFnctInqRq/v1}ServiceBody"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DbCardFnctInqRq", propOrder = {
    "serviceHeader",
    "signon",
    "serviceBody"
})
public class DbCardFnctInqRq {

    @XmlElement(name = "ServiceHeader", required = true)
    protected ServiceHeaderType serviceHeader;
    @XmlElement(name = "Signon", required = true)
    protected SignonType signon;
    @XmlElement(name = "ServiceBody", required = true)
    protected ServiceBody serviceBody;

    /**
     * 取得 serviceHeader 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link ServiceHeaderType }
     *     
     */
    public ServiceHeaderType getServiceHeader() {
        return serviceHeader;
    }

    /**
     * 設定 serviceHeader 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceHeaderType }
     *     
     */
    public void setServiceHeader(ServiceHeaderType value) {
        this.serviceHeader = value;
    }

    /**
     * 取得 signon 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link SignonType }
     *     
     */
    public SignonType getSignon() {
        return signon;
    }

    /**
     * 設定 signon 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link SignonType }
     *     
     */
    public void setSignon(SignonType value) {
        this.signon = value;
    }

    /**
     * 取得 serviceBody 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link ServiceBody }
     *     
     */
    public ServiceBody getServiceBody() {
        return serviceBody;
    }

    /**
     * 設定 serviceBody 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceBody }
     *     
     */
    public void setServiceBody(ServiceBody value) {
        this.serviceBody = value;
    }

}
