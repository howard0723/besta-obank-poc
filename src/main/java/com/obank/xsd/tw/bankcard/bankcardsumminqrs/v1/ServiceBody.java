
package com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ServiceBody complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="ServiceBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RtrnCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RtrnDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1}BankCardRec" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1}ControlRec" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceBody", propOrder = {
    "rtrnCode",
    "rtrnDesc",
    "bankCardRec",
    "controlRec"
})
public class ServiceBody {

    @XmlElement(name = "RtrnCode", required = true)
    protected String rtrnCode;
    @XmlElement(name = "RtrnDesc")
    protected String rtrnDesc;
    @XmlElement(name = "BankCardRec")
    protected List<BankCardRec> bankCardRec;
    @XmlElement(name = "ControlRec")
    protected ControlRec controlRec;

    /**
     * 取得 rtrnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRtrnCode() {
        return rtrnCode;
    }

    /**
     * 設定 rtrnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRtrnCode(String value) {
        this.rtrnCode = value;
    }

    /**
     * 取得 rtrnDesc 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRtrnDesc() {
        return rtrnDesc;
    }

    /**
     * 設定 rtrnDesc 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRtrnDesc(String value) {
        this.rtrnDesc = value;
    }

    /**
     * Gets the value of the bankCardRec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bankCardRec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBankCardRec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BankCardRec }
     * 
     * 
     */
    public List<BankCardRec> getBankCardRec() {
        if (bankCardRec == null) {
            bankCardRec = new ArrayList<BankCardRec>();
        }
        return this.bankCardRec;
    }

    /**
     * 取得 controlRec 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link ControlRec }
     *     
     */
    public ControlRec getControlRec() {
        return controlRec;
    }

    /**
     * 設定 controlRec 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlRec }
     *     
     */
    public void setControlRec(ControlRec value) {
        this.controlRec = value;
    }

}
