
package com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CrBankInfo complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CrBankInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CrBankId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CrAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrBankInfo", propOrder = {
    "crBankId",
    "crAcctNo"
})
public class CrBankInfo {

    @XmlElement(name = "CrBankId", required = true)
    protected String crBankId;
    @XmlElement(name = "CrAcctNo", required = true)
    protected String crAcctNo;

    /**
     * 取得 crBankId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrBankId() {
        return crBankId;
    }

    /**
     * 設定 crBankId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrBankId(String value) {
        this.crBankId = value;
    }

    /**
     * 取得 crAcctNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrAcctNo() {
        return crAcctNo;
    }

    /**
     * 設定 crAcctNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrAcctNo(String value) {
        this.crAcctNo = value;
    }

}
