
package com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>BankCardRec complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="BankCardRec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIFNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustPermId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustChnName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustEngName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BirthDay" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MobNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AddrZip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MailAddr1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MailAddr2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardFace" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardFaceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardGrade" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="GrpNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ElekCardType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DayLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="MnthLmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="NPDFundXfer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="XbrdrWdl" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SmartPayFlg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DlvrMthd" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DlvrMthdName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DlvrBrch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DlvrBrchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DbAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HCE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SMSFlg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PushAppFlg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="eMailFlg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SMSAmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="eMailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1}DbAcctInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1}CrBankInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="WalletNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardStat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardIssueStat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EffDateFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VldDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AppDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RegrDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegrNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardAttr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardAttrName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RelnCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RelnCodeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PriCIFNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PriCustId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PriCardNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SrcId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TlrId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PriBirthDay" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PriMobNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CardLmtShrtAmt" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="EffDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MakeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Img" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ImgFlg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankCardRec", propOrder = {
    "cifNo",
    "custPermId",
    "custChnName",
    "custEngName",
    "birthDay",
    "mobNo",
    "addrZip",
    "mailAddr1",
    "mailAddr2",
    "cardType",
    "cardTypeName",
    "cardFace",
    "cardFaceName",
    "cardGrade",
    "grpNo",
    "elekCardType",
    "dayLmt",
    "mnthLmt",
    "npdFundXfer",
    "xbrdrWdl",
    "smartPayFlg",
    "dlvrMthd",
    "dlvrMthdName",
    "dlvrBrch",
    "dlvrBrchName",
    "dbAcctNo",
    "hce",
    "smsFlg",
    "pushAppFlg",
    "eMailFlg",
    "smsAmt",
    "eMailAddr",
    "dbAcctInfo",
    "crBankInfo",
    "cardNo",
    "walletNo",
    "cardStat",
    "cardIssueStat",
    "effDateFrom",
    "vldDate",
    "appDate",
    "regrDate",
    "regrNo",
    "cardAttr",
    "cardAttrName",
    "relnCode",
    "relnCodeName",
    "priCIFNo",
    "priCustId",
    "priCardNo",
    "srcId",
    "tlrId",
    "priBirthDay",
    "priMobNo",
    "cardLmtShrtAmt",
    "effDate",
    "makeDate",
    "img",
    "imgFlg"
})
public class BankCardRec {

    @XmlElement(name = "CIFNo", required = true)
    protected String cifNo;
    @XmlElement(name = "CustPermId", required = true)
    protected String custPermId;
    @XmlElement(name = "CustChnName", required = true)
    protected String custChnName;
    @XmlElement(name = "CustEngName", required = true)
    protected String custEngName;
    @XmlElement(name = "BirthDay", required = true)
    protected String birthDay;
    @XmlElement(name = "MobNo", required = true)
    protected String mobNo;
    @XmlElement(name = "AddrZip", required = true)
    protected String addrZip;
    @XmlElement(name = "MailAddr1", required = true)
    protected String mailAddr1;
    @XmlElement(name = "MailAddr2", required = true)
    protected String mailAddr2;
    @XmlElement(name = "CardType", required = true)
    protected String cardType;
    @XmlElement(name = "CardTypeName", required = true)
    protected String cardTypeName;
    @XmlElement(name = "CardFace", required = true)
    protected String cardFace;
    @XmlElement(name = "CardFaceName", required = true)
    protected String cardFaceName;
    @XmlElement(name = "CardGrade", required = true)
    protected String cardGrade;
    @XmlElement(name = "GrpNo", required = true)
    protected String grpNo;
    @XmlElement(name = "ElekCardType", required = true)
    protected String elekCardType;
    @XmlElement(name = "DayLmt", required = true)
    protected BigDecimal dayLmt;
    @XmlElement(name = "MnthLmt", required = true)
    protected BigDecimal mnthLmt;
    @XmlElement(name = "NPDFundXfer", required = true)
    protected String npdFundXfer;
    @XmlElement(name = "XbrdrWdl", required = true)
    protected String xbrdrWdl;
    @XmlElement(name = "SmartPayFlg", required = true)
    protected String smartPayFlg;
    @XmlElement(name = "DlvrMthd", required = true)
    protected String dlvrMthd;
    @XmlElement(name = "DlvrMthdName", required = true)
    protected String dlvrMthdName;
    @XmlElement(name = "DlvrBrch")
    protected String dlvrBrch;
    @XmlElement(name = "DlvrBrchName")
    protected String dlvrBrchName;
    @XmlElement(name = "DbAcctNo", required = true)
    protected String dbAcctNo;
    @XmlElement(name = "HCE", required = true)
    protected String hce;
    @XmlElement(name = "SMSFlg", required = true)
    protected String smsFlg;
    @XmlElement(name = "PushAppFlg", required = true)
    protected String pushAppFlg;
    @XmlElement(required = true)
    protected String eMailFlg;
    @XmlElement(name = "SMSAmt", required = true)
    protected BigDecimal smsAmt;
    @XmlElement(required = true)
    protected String eMailAddr;
    @XmlElement(name = "DbAcctInfo")
    protected List<DbAcctInfo> dbAcctInfo;
    @XmlElement(name = "CrBankInfo")
    protected List<CrBankInfo> crBankInfo;
    @XmlElement(name = "CardNo", required = true)
    protected String cardNo;
    @XmlElement(name = "WalletNo")
    protected String walletNo;
    @XmlElement(name = "CardStat", required = true)
    protected String cardStat;
    @XmlElement(name = "CardIssueStat", required = true)
    protected String cardIssueStat;
    @XmlElement(name = "EffDateFrom")
    protected String effDateFrom;
    @XmlElement(name = "VldDate")
    protected String vldDate;
    @XmlElement(name = "AppDate", required = true)
    protected String appDate;
    @XmlElement(name = "RegrDate")
    protected String regrDate;
    @XmlElement(name = "RegrNo")
    protected String regrNo;
    @XmlElement(name = "CardAttr", required = true)
    protected String cardAttr;
    @XmlElement(name = "CardAttrName", required = true)
    protected String cardAttrName;
    @XmlElement(name = "RelnCode", required = true)
    protected String relnCode;
    @XmlElement(name = "RelnCodeName", required = true)
    protected String relnCodeName;
    @XmlElement(name = "PriCIFNo", required = true)
    protected String priCIFNo;
    @XmlElement(name = "PriCustId", required = true)
    protected String priCustId;
    @XmlElement(name = "PriCardNo", required = true)
    protected String priCardNo;
    @XmlElement(name = "SrcId", required = true)
    protected String srcId;
    @XmlElement(name = "TlrId", required = true)
    protected String tlrId;
    @XmlElement(name = "PriBirthDay", required = true)
    protected String priBirthDay;
    @XmlElement(name = "PriMobNo", required = true)
    protected String priMobNo;
    @XmlElement(name = "CardLmtShrtAmt", required = true)
    protected BigDecimal cardLmtShrtAmt;
    @XmlElement(name = "EffDate")
    protected String effDate;
    @XmlElement(name = "MakeDate")
    protected String makeDate;
    @XmlElement(name = "Img", required = true)
    protected String img;
    @XmlElement(name = "ImgFlg", required = true)
    protected String imgFlg;

    /**
     * 取得 cifNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFNo() {
        return cifNo;
    }

    /**
     * 設定 cifNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFNo(String value) {
        this.cifNo = value;
    }

    /**
     * 取得 custPermId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPermId() {
        return custPermId;
    }

    /**
     * 設定 custPermId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPermId(String value) {
        this.custPermId = value;
    }

    /**
     * 取得 custChnName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustChnName() {
        return custChnName;
    }

    /**
     * 設定 custChnName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustChnName(String value) {
        this.custChnName = value;
    }

    /**
     * 取得 custEngName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEngName() {
        return custEngName;
    }

    /**
     * 設定 custEngName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEngName(String value) {
        this.custEngName = value;
    }

    /**
     * 取得 birthDay 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * 設定 birthDay 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthDay(String value) {
        this.birthDay = value;
    }

    /**
     * 取得 mobNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobNo() {
        return mobNo;
    }

    /**
     * 設定 mobNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobNo(String value) {
        this.mobNo = value;
    }

    /**
     * 取得 addrZip 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddrZip() {
        return addrZip;
    }

    /**
     * 設定 addrZip 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddrZip(String value) {
        this.addrZip = value;
    }

    /**
     * 取得 mailAddr1 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailAddr1() {
        return mailAddr1;
    }

    /**
     * 設定 mailAddr1 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailAddr1(String value) {
        this.mailAddr1 = value;
    }

    /**
     * 取得 mailAddr2 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailAddr2() {
        return mailAddr2;
    }

    /**
     * 設定 mailAddr2 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailAddr2(String value) {
        this.mailAddr2 = value;
    }

    /**
     * 取得 cardType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * 設定 cardType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * 取得 cardTypeName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTypeName() {
        return cardTypeName;
    }

    /**
     * 設定 cardTypeName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTypeName(String value) {
        this.cardTypeName = value;
    }

    /**
     * 取得 cardFace 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardFace() {
        return cardFace;
    }

    /**
     * 設定 cardFace 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardFace(String value) {
        this.cardFace = value;
    }

    /**
     * 取得 cardFaceName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardFaceName() {
        return cardFaceName;
    }

    /**
     * 設定 cardFaceName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardFaceName(String value) {
        this.cardFaceName = value;
    }

    /**
     * 取得 cardGrade 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardGrade() {
        return cardGrade;
    }

    /**
     * 設定 cardGrade 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardGrade(String value) {
        this.cardGrade = value;
    }

    /**
     * 取得 grpNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrpNo() {
        return grpNo;
    }

    /**
     * 設定 grpNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrpNo(String value) {
        this.grpNo = value;
    }

    /**
     * 取得 elekCardType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElekCardType() {
        return elekCardType;
    }

    /**
     * 設定 elekCardType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElekCardType(String value) {
        this.elekCardType = value;
    }

    /**
     * 取得 dayLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDayLmt() {
        return dayLmt;
    }

    /**
     * 設定 dayLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDayLmt(BigDecimal value) {
        this.dayLmt = value;
    }

    /**
     * 取得 mnthLmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMnthLmt() {
        return mnthLmt;
    }

    /**
     * 設定 mnthLmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMnthLmt(BigDecimal value) {
        this.mnthLmt = value;
    }

    /**
     * 取得 npdFundXfer 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNPDFundXfer() {
        return npdFundXfer;
    }

    /**
     * 設定 npdFundXfer 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNPDFundXfer(String value) {
        this.npdFundXfer = value;
    }

    /**
     * 取得 xbrdrWdl 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXbrdrWdl() {
        return xbrdrWdl;
    }

    /**
     * 設定 xbrdrWdl 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXbrdrWdl(String value) {
        this.xbrdrWdl = value;
    }

    /**
     * 取得 smartPayFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartPayFlg() {
        return smartPayFlg;
    }

    /**
     * 設定 smartPayFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartPayFlg(String value) {
        this.smartPayFlg = value;
    }

    /**
     * 取得 dlvrMthd 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlvrMthd() {
        return dlvrMthd;
    }

    /**
     * 設定 dlvrMthd 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlvrMthd(String value) {
        this.dlvrMthd = value;
    }

    /**
     * 取得 dlvrMthdName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlvrMthdName() {
        return dlvrMthdName;
    }

    /**
     * 設定 dlvrMthdName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlvrMthdName(String value) {
        this.dlvrMthdName = value;
    }

    /**
     * 取得 dlvrBrch 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlvrBrch() {
        return dlvrBrch;
    }

    /**
     * 設定 dlvrBrch 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlvrBrch(String value) {
        this.dlvrBrch = value;
    }

    /**
     * 取得 dlvrBrchName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlvrBrchName() {
        return dlvrBrchName;
    }

    /**
     * 設定 dlvrBrchName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlvrBrchName(String value) {
        this.dlvrBrchName = value;
    }

    /**
     * 取得 dbAcctNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDbAcctNo() {
        return dbAcctNo;
    }

    /**
     * 設定 dbAcctNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDbAcctNo(String value) {
        this.dbAcctNo = value;
    }

    /**
     * 取得 hce 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHCE() {
        return hce;
    }

    /**
     * 設定 hce 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHCE(String value) {
        this.hce = value;
    }

    /**
     * 取得 smsFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSFlg() {
        return smsFlg;
    }

    /**
     * 設定 smsFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSFlg(String value) {
        this.smsFlg = value;
    }

    /**
     * 取得 pushAppFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPushAppFlg() {
        return pushAppFlg;
    }

    /**
     * 設定 pushAppFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPushAppFlg(String value) {
        this.pushAppFlg = value;
    }

    /**
     * 取得 eMailFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMailFlg() {
        return eMailFlg;
    }

    /**
     * 設定 eMailFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMailFlg(String value) {
        this.eMailFlg = value;
    }

    /**
     * 取得 smsAmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSMSAmt() {
        return smsAmt;
    }

    /**
     * 設定 smsAmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSMSAmt(BigDecimal value) {
        this.smsAmt = value;
    }

    /**
     * 取得 eMailAddr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMailAddr() {
        return eMailAddr;
    }

    /**
     * 設定 eMailAddr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMailAddr(String value) {
        this.eMailAddr = value;
    }

    /**
     * Gets the value of the dbAcctInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dbAcctInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDbAcctInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DbAcctInfo }
     * 
     * 
     */
    public List<DbAcctInfo> getDbAcctInfo() {
        if (dbAcctInfo == null) {
            dbAcctInfo = new ArrayList<DbAcctInfo>();
        }
        return this.dbAcctInfo;
    }

    /**
     * Gets the value of the crBankInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the crBankInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCrBankInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CrBankInfo }
     * 
     * 
     */
    public List<CrBankInfo> getCrBankInfo() {
        if (crBankInfo == null) {
            crBankInfo = new ArrayList<CrBankInfo>();
        }
        return this.crBankInfo;
    }

    /**
     * 取得 cardNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * 設定 cardNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * 取得 walletNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWalletNo() {
        return walletNo;
    }

    /**
     * 設定 walletNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWalletNo(String value) {
        this.walletNo = value;
    }

    /**
     * 取得 cardStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardStat() {
        return cardStat;
    }

    /**
     * 設定 cardStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardStat(String value) {
        this.cardStat = value;
    }

    /**
     * 取得 cardIssueStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardIssueStat() {
        return cardIssueStat;
    }

    /**
     * 設定 cardIssueStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardIssueStat(String value) {
        this.cardIssueStat = value;
    }

    /**
     * 取得 effDateFrom 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffDateFrom() {
        return effDateFrom;
    }

    /**
     * 設定 effDateFrom 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffDateFrom(String value) {
        this.effDateFrom = value;
    }

    /**
     * 取得 vldDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVldDate() {
        return vldDate;
    }

    /**
     * 設定 vldDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVldDate(String value) {
        this.vldDate = value;
    }

    /**
     * 取得 appDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppDate() {
        return appDate;
    }

    /**
     * 設定 appDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppDate(String value) {
        this.appDate = value;
    }

    /**
     * 取得 regrDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegrDate() {
        return regrDate;
    }

    /**
     * 設定 regrDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegrDate(String value) {
        this.regrDate = value;
    }

    /**
     * 取得 regrNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegrNo() {
        return regrNo;
    }

    /**
     * 設定 regrNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegrNo(String value) {
        this.regrNo = value;
    }

    /**
     * 取得 cardAttr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAttr() {
        return cardAttr;
    }

    /**
     * 設定 cardAttr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAttr(String value) {
        this.cardAttr = value;
    }

    /**
     * 取得 cardAttrName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAttrName() {
        return cardAttrName;
    }

    /**
     * 設定 cardAttrName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAttrName(String value) {
        this.cardAttrName = value;
    }

    /**
     * 取得 relnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelnCode() {
        return relnCode;
    }

    /**
     * 設定 relnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelnCode(String value) {
        this.relnCode = value;
    }

    /**
     * 取得 relnCodeName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelnCodeName() {
        return relnCodeName;
    }

    /**
     * 設定 relnCodeName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelnCodeName(String value) {
        this.relnCodeName = value;
    }

    /**
     * 取得 priCIFNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriCIFNo() {
        return priCIFNo;
    }

    /**
     * 設定 priCIFNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriCIFNo(String value) {
        this.priCIFNo = value;
    }

    /**
     * 取得 priCustId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriCustId() {
        return priCustId;
    }

    /**
     * 設定 priCustId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriCustId(String value) {
        this.priCustId = value;
    }

    /**
     * 取得 priCardNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriCardNo() {
        return priCardNo;
    }

    /**
     * 設定 priCardNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriCardNo(String value) {
        this.priCardNo = value;
    }

    /**
     * 取得 srcId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcId() {
        return srcId;
    }

    /**
     * 設定 srcId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcId(String value) {
        this.srcId = value;
    }

    /**
     * 取得 tlrId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTlrId() {
        return tlrId;
    }

    /**
     * 設定 tlrId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTlrId(String value) {
        this.tlrId = value;
    }

    /**
     * 取得 priBirthDay 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriBirthDay() {
        return priBirthDay;
    }

    /**
     * 設定 priBirthDay 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriBirthDay(String value) {
        this.priBirthDay = value;
    }

    /**
     * 取得 priMobNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriMobNo() {
        return priMobNo;
    }

    /**
     * 設定 priMobNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriMobNo(String value) {
        this.priMobNo = value;
    }

    /**
     * 取得 cardLmtShrtAmt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardLmtShrtAmt() {
        return cardLmtShrtAmt;
    }

    /**
     * 設定 cardLmtShrtAmt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardLmtShrtAmt(BigDecimal value) {
        this.cardLmtShrtAmt = value;
    }

    /**
     * 取得 effDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffDate() {
        return effDate;
    }

    /**
     * 設定 effDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffDate(String value) {
        this.effDate = value;
    }

    /**
     * 取得 makeDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakeDate() {
        return makeDate;
    }

    /**
     * 設定 makeDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakeDate(String value) {
        this.makeDate = value;
    }

    /**
     * 取得 img 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImg() {
        return img;
    }

    /**
     * 設定 img 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImg(String value) {
        this.img = value;
    }

    /**
     * 取得 imgFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImgFlg() {
        return imgFlg;
    }

    /**
     * 設定 imgFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImgFlg(String value) {
        this.imgFlg = value;
    }

}
