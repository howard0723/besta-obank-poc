
package com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ServiceBody complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="ServiceBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PageNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="NoOfPage" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="InqType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InqKeyId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceBody", propOrder = {
    "pageNo",
    "noOfPage",
    "inqType",
    "inqKeyId"
})
public class ServiceBody {

    @XmlElement(name = "PageNo", required = true)
    protected BigInteger pageNo;
    @XmlElement(name = "NoOfPage", required = true)
    protected BigInteger noOfPage;
    @XmlElement(name = "InqType", required = true)
    protected String inqType;
    @XmlElement(name = "InqKeyId", required = true)
    protected String inqKeyId;

    /**
     * 取得 pageNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNo() {
        return pageNo;
    }

    /**
     * 設定 pageNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNo(BigInteger value) {
        this.pageNo = value;
    }

    /**
     * 取得 noOfPage 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfPage() {
        return noOfPage;
    }

    /**
     * 設定 noOfPage 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfPage(BigInteger value) {
        this.noOfPage = value;
    }

    /**
     * 取得 inqType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInqType() {
        return inqType;
    }

    /**
     * 設定 inqType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInqType(String value) {
        this.inqType = value;
    }

    /**
     * 取得 inqKeyId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInqKeyId() {
        return inqKeyId;
    }

    /**
     * 設定 inqKeyId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInqKeyId(String value) {
        this.inqKeyId = value;
    }

}
