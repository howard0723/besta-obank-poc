
package com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceHeader_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "ServiceHeader");
    private final static QName _Signon_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "Signon");
    private final static QName _BankCardSummInqRs_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "BankCardSummInqRs");
    private final static QName _ServiceBody_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "ServiceBody");
    private final static QName _BankCardRec_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "BankCardRec");
    private final static QName _DbAcctInfo_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "DbAcctInfo");
    private final static QName _CrBankInfo_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "CrBankInfo");
    private final static QName _ControlRec_QNAME = new QName("http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", "ControlRec");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceHeaderType }
     * 
     */
    public ServiceHeaderType createServiceHeaderType() {
        return new ServiceHeaderType();
    }

    /**
     * Create an instance of {@link SignonType }
     * 
     */
    public SignonType createSignonType() {
        return new SignonType();
    }

    /**
     * Create an instance of {@link BankCardSummInqRs }
     * 
     */
    public BankCardSummInqRs createBankCardSummInqRs() {
        return new BankCardSummInqRs();
    }

    /**
     * Create an instance of {@link ServiceBody }
     * 
     */
    public ServiceBody createServiceBody() {
        return new ServiceBody();
    }

    /**
     * Create an instance of {@link BankCardRec }
     * 
     */
    public BankCardRec createBankCardRec() {
        return new BankCardRec();
    }

    /**
     * Create an instance of {@link DbAcctInfo }
     * 
     */
    public DbAcctInfo createDbAcctInfo() {
        return new DbAcctInfo();
    }

    /**
     * Create an instance of {@link CrBankInfo }
     * 
     */
    public CrBankInfo createCrBankInfo() {
        return new CrBankInfo();
    }

    /**
     * Create an instance of {@link ControlRec }
     * 
     */
    public ControlRec createControlRec() {
        return new ControlRec();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceHeaderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "ServiceHeader")
    public JAXBElement<ServiceHeaderType> createServiceHeader(ServiceHeaderType value) {
        return new JAXBElement<ServiceHeaderType>(_ServiceHeader_QNAME, ServiceHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "Signon")
    public JAXBElement<SignonType> createSignon(SignonType value) {
        return new JAXBElement<SignonType>(_Signon_QNAME, SignonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankCardSummInqRs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "BankCardSummInqRs")
    public JAXBElement<BankCardSummInqRs> createBankCardSummInqRs(BankCardSummInqRs value) {
        return new JAXBElement<BankCardSummInqRs>(_BankCardSummInqRs_QNAME, BankCardSummInqRs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceBody }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "ServiceBody")
    public JAXBElement<ServiceBody> createServiceBody(ServiceBody value) {
        return new JAXBElement<ServiceBody>(_ServiceBody_QNAME, ServiceBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankCardRec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "BankCardRec")
    public JAXBElement<BankCardRec> createBankCardRec(BankCardRec value) {
        return new JAXBElement<BankCardRec>(_BankCardRec_QNAME, BankCardRec.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DbAcctInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "DbAcctInfo")
    public JAXBElement<DbAcctInfo> createDbAcctInfo(DbAcctInfo value) {
        return new JAXBElement<DbAcctInfo>(_DbAcctInfo_QNAME, DbAcctInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrBankInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "CrBankInfo")
    public JAXBElement<CrBankInfo> createCrBankInfo(CrBankInfo value) {
        return new JAXBElement<CrBankInfo>(_CrBankInfo_QNAME, CrBankInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ControlRec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRs/v1", name = "ControlRec")
    public JAXBElement<ControlRec> createControlRec(ControlRec value) {
        return new JAXBElement<ControlRec>(_ControlRec_QNAME, ControlRec.class, null, value);
    }

}
