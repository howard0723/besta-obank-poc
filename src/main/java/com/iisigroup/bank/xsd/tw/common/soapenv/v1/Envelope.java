
package com.iisigroup.bank.xsd.tw.common.soapenv.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Envelope complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="Envelope"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1}Header"/&gt;
 *         &lt;element ref="{http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1}Body"/&gt;
 *         &lt;element ref="{http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1}Fault" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Envelope", propOrder = {
    "header",
    "body",
    "fault"
})
public class Envelope {

    @XmlElement(name = "Header", namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", required = true)
    protected Header header;
    @XmlElement(name = "Body", namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", required = true)
    protected Body body;
    @XmlElement(name = "Fault", namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1")
    protected Fault fault;

    /**
     * 取得 header 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * 設定 header 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * 取得 body 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link Body }
     *     
     */
    public Body getBody() {
        return body;
    }

    /**
     * 設定 body 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link Body }
     *     
     */
    public void setBody(Body value) {
        this.body = value;
    }

    /**
     * 取得 fault 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link Fault }
     *     
     */
    public Fault getFault() {
        return fault;
    }

    /**
     * 設定 fault 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link Fault }
     *     
     */
    public void setFault(Fault value) {
        this.fault = value;
    }

}
