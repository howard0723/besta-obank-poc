
package com.iisigroup.bank.xsd.tw.common.soapenv.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>detail complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="detail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorcode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="errorstring" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "detail", propOrder = {
    "errorcode",
    "errorstring"
})
public class Detail {

    @XmlElement(required = true)
    protected String errorcode;
    @XmlElement(required = true)
    protected String errorstring;

    /**
     * 取得 errorcode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     * 設定 errorcode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorcode(String value) {
        this.errorcode = value;
    }

    /**
     * 取得 errorstring 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorstring() {
        return errorstring;
    }

    /**
     * 設定 errorstring 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorstring(String value) {
        this.errorstring = value;
    }

}
