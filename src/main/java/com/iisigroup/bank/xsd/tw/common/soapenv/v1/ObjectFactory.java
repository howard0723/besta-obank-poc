
package com.iisigroup.bank.xsd.tw.common.soapenv.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.iisigroup.bank.xsd.tw.common.soapenv.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Envelope_QNAME = new QName("http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", "Envelope");
    private final static QName _Header_QNAME = new QName("http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", "Header");
    private final static QName _Body_QNAME = new QName("http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", "Body");
    private final static QName _Fault_QNAME = new QName("http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", "Fault");
    private final static QName _Detail_QNAME = new QName("http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", "detail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.iisigroup.bank.xsd.tw.common.soapenv.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Envelope }
     * 
     */
    public Envelope createEnvelope() {
        return new Envelope();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link Body }
     * 
     */
    public Body createBody() {
        return new Body();
    }

    /**
     * Create an instance of {@link Fault }
     * 
     */
    public Fault createFault() {
        return new Fault();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Envelope }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", name = "Envelope")
    public JAXBElement<Envelope> createEnvelope(Envelope value) {
        return new JAXBElement<Envelope>(_Envelope_QNAME, Envelope.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Header }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", name = "Header")
    public JAXBElement<Header> createHeader(Header value) {
        return new JAXBElement<Header>(_Header_QNAME, Header.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Body }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", name = "Body")
    public JAXBElement<Body> createBody(Body value) {
        return new JAXBElement<Body>(_Body_QNAME, Body.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Fault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", name = "Fault")
    public JAXBElement<Fault> createFault(Fault value) {
        return new JAXBElement<Fault>(_Fault_QNAME, Fault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Detail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1", name = "detail")
    public JAXBElement<Detail> createDetail(Detail value) {
        return new JAXBElement<Detail>(_Detail_QNAME, Detail.class, null, value);
    }

}
