package org.mycompany.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.camel.CamelContext;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.mycompany.exception.DataNotFoundException;
import org.mycompany.soap.impl.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInfo;
import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;

@RestController
public class ObankController {
	@Autowired
	private CamelContext camelContext;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping("/customerdata")
	public ResponseEntity<Map<String, Object>> customerdata(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid,
			@RequestParam(required = false, value = "customerId") String customerId,
			@RequestParam(required = false, value = "CIFNo") String CIFNo) {
		
			System.out.println("customerdata......." + customerId);
			JSONObject json = null;
			Map<String, Object> rtn = new LinkedHashMap<String,Object>();
	    	Map<String,Object> responseStatus = new LinkedHashMap<String,Object>();
			try {					
//				String url = "http://www.dneonline.com/calculator.asmx?wsdl";
//				SOAPMessage soapMessage = genSoapMessageForThirdPartyApi();
				CustProfInqRs custProfInqRs = null;			  					
				CustomerService cs = new CustomerService();
				
				Map<String ,String> params = new HashMap<String ,String>();
				params.put("customerId", customerId);
				params.put("CIFNo", CIFNo);				
				custProfInqRs =  cs.getCustomerData(params);
								
		    	if("00000".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode())) {
		    		json = genJSon(custProfInqRs);
		    		return new ResponseEntity<Map<String, Object>>(json.toMap(),HttpStatus.OK);		    		
		    	}else if ("E1626".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode())) {
		    		logger.error("查無資料");
		    		responseStatus.put("code", "1001");
					responseStatus.put("description", "查無資料");
					rtn.put("responseStatus", responseStatus);
					return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.NOT_FOUND);		    		
				} else {
					logger.error("unknow response rtn code.");
					responseStatus.put("code", "9999");
					responseStatus.put("description", "服務異常");
					rtn.put("responseStatus", responseStatus);
					return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
				}				


			} catch (Throwable e) {
				logger.error(e.getMessage(),e);
				responseStatus.put("code", "9999");
				responseStatus.put("description", "服務異常");
				rtn.put("responseStatus", responseStatus);
				return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	public JSONObject genJSon(CustProfInqRs custProfInqRs) {
		JSONObject json = new JSONObject();
		CustProfInfo custProfInfo = custProfInqRs.getServiceBody().getCustProfInfo();
		json.put("CIFNo", custProfInfo.getCIFNo());
		json.put("customerId", custProfInfo.getMnem());
		json.put("chineseName", custProfInfo.getCustChnName());
		json.put("nickName", custProfInfo.getCustShrtName());
		json.put("birthday", custProfInfo.getBirthDay());
		json.put("cellphoneNo", custProfInfo.getMobNo1());
		json.put("email", custProfInfo.getEMailAddr1());
		JSONObject addressNodeJson = new JSONObject();
		JSONObject streetNodeJson = new JSONObject();
		json.put("address", addressNodeJson);
		addressNodeJson.put("street", streetNodeJson);
		streetNodeJson.put("global", custProfInfo.getMailAddr().getSt().getGBSt());
		streetNodeJson.put("taiwan", custProfInfo.getMailAddr().getSt().getTWSt());
		
		JSONObject townNodeJson = new JSONObject();
		townNodeJson.put("global", custProfInfo.getMailAddr().getTownCnty().getGBTownCnty());
		townNodeJson.put("taiwan", custProfInfo.getMailAddr().getTownCnty().getTWTownCnty());
		
		json.put("townCountry",townNodeJson);
		
		JSONObject zipNodeJson = new JSONObject();
		zipNodeJson.put("global", custProfInfo.getMailAddr().getAddrZip().getGBAddrZip());
		zipNodeJson.put("taiwan", custProfInfo.getMailAddr().getAddrZip().getTWAddrZip());
		
		json.put("zip", zipNodeJson);
		json.put("country", custProfInfo.getMailAddr().getCntry());
		System.out.println("json::::" + json);
		return json;
	}
	
	public String sendHttpRequest(SOAPMessage soapMessage) {
		String responseBody = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			soapMessage.writeTo(out);
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String strMsg = new String(out.toByteArray());
		System.out.println("***********soap req message ==" + strMsg);
		
		
		String endpoint = "https://testsfdcesb.o-bank.com/BusinessServices/ServiceMediationService.serviceagent/ServiceMediationOperation";
		
		RequestEntity entity = new StringRequestEntity(strMsg);
		
		
		
		  // HTTP POST 請求物件
		PostMethod post = new PostMethod(endpoint);
		post.setRequestEntity(entity);
		post.setRequestHeader("SOAPAction", "/BusinessServices/Customer/CustProfInq/V1/Operation");
		post.setRequestHeader("Content-type", "application/soap+xml; charset=utf-8");
		post.setRequestHeader("Content-Length", entity.getContentLength() + "");
		HttpClient httpclient = new HttpClient();
		try {
		    int result = 0;
			try {
				result = httpclient.executeMethod(post);
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.out.println("Response status code: " + result);
		    System.out.println("Response body: ");
		    responseBody = post.getResponseBodyAsString();
		    System.out.println(responseBody);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		    post.releaseConnection();
		}
	   return responseBody;
	}
	
	private String convertXmltoJson() {
		String jsonStr = "";
		
		
		return jsonStr;
	}
	
	private String createSOAPRequest(String url , SOAPMessage soapMessage) {
		String result = null;
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			
			SOAPMessage soapResponse = soapConnection.call(soapMessage, url);

			// Process the SOAP Response
			result = printSOAPResponse(soapResponse);
			soapConnection.close();			
		} catch (Exception e) {
			System.err.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage genSoapMessageForThirdPartyApi() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		
		MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "http://tempuri.org/Add");
				
		SOAPPart soapPart = soapMessage.getSOAPPart();


		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("", "http://tempuri.org/");

//		// SOAP Header
//		SOAPHeader soapHeader = envelope.getHeader();
//		SOAPElement v1Header = soapHeader.addChildElement("Header", "v1");
//		v1Header.setAttribute("v1:mustUnderstand", "1");
//		SOAPElement ChannelID = v1Header.addChildElement("ChannelID");
//		ChannelID.addTextNode("ESB");
//		SOAPElement ServiceDomain = v1Header.addChildElement("ServiceDomain");
//		ServiceDomain.addTextNode("Customer");
//		SOAPElement OperationName = v1Header.addChildElement("OperationName");
//		OperationName.addTextNode("CustProfInq");
//		SOAPElement ConsumerID = v1Header.addChildElement("ConsumerID");
//		ConsumerID.addTextNode("TWESB");
//		SOAPElement TransactionID = v1Header.addChildElement("TransactionID");
//		TransactionID.addTextNode("a1b2c320190718201907181036501231");  //Generate a new Transaction id
//		SOAPElement RqTimestamp = v1Header.addChildElement("RqTimestamp");
//		RqTimestamp.addTextNode("2019-07-18T12:04:53");  //Generate a new request time.
		
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Add", "");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("intA","");
		soapBodyElem1.addTextNode("1");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("intB","");
		soapBodyElem2.addTextNode("2");
		
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}
	
	
	
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage genSoapMessage(String customerId) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
	       
        MimeHeaders headers = soapMessage.getMimeHeaders(); 
        headers.addHeader("Content-Type", "text/soap+xml");
        headers.addHeader("SOAPAction", "/BusinessServices/Customer/CustProfInq/V1/Operation");
        
		SOAPPart soapPart = soapMessage.getSOAPPart();

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		envelope.addNamespaceDeclaration("v11", "http://www.obank.com/xsd/TW/Customer/CustProfInqRq/v1");

		// SOAP Header
		SOAPHeader soapHeader = envelope.getHeader();
		SOAPElement v1Header = soapHeader.addChildElement("Header", "v1");
		v1Header.setAttribute("v1:mustUnderstand", "1");
		SOAPElement ChannelID = v1Header.addChildElement("ChannelID");
		ChannelID.addTextNode("ESB");
		SOAPElement ServiceDomain = v1Header.addChildElement("ServiceDomain");
		ServiceDomain.addTextNode("Customer");
		SOAPElement OperationName = v1Header.addChildElement("OperationName");
		OperationName.addTextNode("CustProfInq");
		SOAPElement ConsumerID = v1Header.addChildElement("ConsumerID");
		ConsumerID.addTextNode("TWESB");
		SOAPElement TransactionID = v1Header.addChildElement("TransactionID");
		TransactionID.addTextNode("a1b2c320190718201907181036501231");  //Generate a new Transaction id
		SOAPElement RqTimestamp = v1Header.addChildElement("RqTimestamp");
		RqTimestamp.addTextNode("2019-07-18T12:04:53");  //Generate a new request time.
		
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement CustProfInqRq = soapBody.addChildElement("CustProfInqRq", "v11");
		SOAPElement ServiceHeader = CustProfInqRq.addChildElement("ServiceHeader", "v11");
		SOAPElement TxnId = ServiceHeader.addChildElement("TxnId", "v11");
		TxnId.addTextNode("XX001");
		SOAPElement TxnNo = ServiceHeader.addChildElement("TxnNo", "v11");
		TxnNo.addTextNode("T0982323");
		
		SOAPElement Signon = CustProfInqRq.addChildElement("Signon", "v11");
		SOAPElement CustId = Signon.addChildElement("CustId", "v11");
		CustId.addTextNode("IFUTBSF0001");
		SOAPElement AuthToken = Signon.addChildElement("AuthToken", "v11");
		AuthToken.addTextNode("0");

		SOAPElement ServiceBody = CustProfInqRq.addChildElement("ServiceBody", "v11");
		
		//<!--You have a CHOICE of the next 2 items at this level-->
		SOAPElement CustPermId = ServiceBody.addChildElement("CustPermId", "v11");
		CustPermId.addTextNode(customerId);  // param by CustPermId
//		SOAPElement CIFNo = ServiceBody.addChildElement("CIFNo", "v11");
//		CIFNo.addTextNode("1000004844");  // param by CIFNo
		
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private static String printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.println("\nResponse SOAP Message = ");
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		transformer.transform(sourceContent, result);
		return writer == null ? null : writer.toString();
	}
	
	@RequestMapping("/index")
	public String test(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid) {
		System.out.println("Test....................." + camelContext);
		
		
		
//		if (!"accept:application/json;charset=UTF-8".equals(contentType)) {
//			return null;
//		}
//		String transactionId = clientID + getDate() + uuid;
//		System.out.print("transactionId:::" + transactionId);

//		try {
//			String endpoint = "http://www.dneonline.com/calculator.asmx?wsdl";
//
//			Service service = new Service();
//			Call call = (Call) service.createCall();
//
//			call.setTargetEndpointAddress(endpoint);
//			call.setUseSOAPAction(true);
//			call.setSOAPActionURI("http://tempuri.org/Add");
//			call.setOperationName(new QName("http://tempuri.org/", "Add"));
//			call.addParameter("intA", org.apache.axis.encoding.XMLType.XSD_INT, javax.xml.rpc.ParameterMode.IN);// 介面的引數
//			call.addParameter("intB", org.apache.axis.encoding.XMLType.XSD_INT, javax.xml.rpc.ParameterMode.IN);// 介面的引數
//			
//			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);// 設定返回型別
//			System.out.println(call);
//			String result = (String) call.invoke(new Object[] { 1 , 2 });
//			// 給方法傳遞引數,並且呼叫方法
//			System.out.println("result is " + result);
//			
////			String endpoint = "http://10.7.21.186:8080/webservice/commonService?wsdl";
////
////			Service service = new Service();
////			Call call = (Call) service.createCall();
////			
////			call.setTargetEndpointAddress(endpoint);
//////			call.setSOAPActionURI("http://10.7.21.186:8080/webservice/commonService");
////			call.setOperationName(new QName("http://service.example.com", "sayHello"));
////			call.addParameter("userName", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);// 介面的引數
////			
////			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);// 設定返回型別
////			String result = (String) call.invoke(new Object[] {"howard"});
////			// 給方法傳遞引數,並且呼叫方法
////			System.out.println("result is " + result);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

//		try {
//			String routeID = "activemq-route";
//			camelContext.startRoute(routeID);
//			Thread.sleep(1000);
//			camelContext.stopRoute(routeID);
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return "Success";
	}

	
	
	
	
	
	
	private String getDate() {
		// Get current date time
		LocalDateTime now = LocalDateTime.now();
		System.out.println("Before : " + now);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

	@SuppressWarnings("resource")
	public static void executeSpringDSL(String routeId) throws Exception {
		new ClassPathXmlApplicationContext("classpath:spring/camel-context.xml").getBean("camel", CamelContext.class)
				.startRoute(routeId);
		Thread.sleep(2000);
		new ClassPathXmlApplicationContext("classpath:spring/camel-context.xml").getBean("camel", CamelContext.class)
				.stop();
	}
}
