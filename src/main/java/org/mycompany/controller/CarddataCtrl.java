package org.mycompany.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mycompany.exception.DataNotFoundException;
import org.mycompany.soap.impl.BankCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarddataCtrl {

	protected BankCardService bandCard = new BankCardService();
	private Logger logger = LoggerFactory.getLogger(getClass());

	@GetMapping("/carddata/{customerId}")
	public ResponseEntity<Map<String, Object>> carddata(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@PathVariable("customerId") String customerId) {
		Map<String, Object> rtn = new LinkedHashMap<String, Object>();
		Map<String, Object> responseStatus = new LinkedHashMap<String, Object>();
		try {
			Map<String,Object> card = bandCard.getCards(customerId, new Date());
			List<Map<String,Object>> cards = new ArrayList<Map<String,Object>>();
			cards.add(card);
			
			rtn.put("cards", cards);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.OK);
		} catch (DataNotFoundException e) {
			logger.error(e.getMessage(), e);
			responseStatus.put("code", "1001");
			responseStatus.put("description", "查無資料");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.NOT_FOUND);
		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
			responseStatus.put("code", "9999");
			responseStatus.put("description", "服務異常");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
