package org.mycompany.soap.impl;

import java.util.Date;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.mycompany.soap.AbstractSaopService;

import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.DbCardFnctInqRs;
import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.ObjectFactory;

public class DebitCardFnctIngService extends AbstractSaopService<DbCardFnctInqRs> {
	
	private String tagName = "DbCardFnctInqRs";
	@Override
	public String getTagName() {
		return tagName;
	}
	@Override
	public String getTestFile() {
		return "C:\\Users\\A60010\\Desktop\\obank\\carddata\\res\\DbCardFnctInq_sample_response.xml";
	}
	@Override
	public Class<?> getObjectFactory() {
		return ObjectFactory.class;
	}
	@Override
	public String getAction() {
		return "/BusinessServices/DebitCard/DbCardFnctInq/V1/Operation";
	}
	@Override
	public SOAPMessage createSOAPMessage(Date date, String value) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

        /*
			<v11:ServiceBody>
				<v11:InqPageNo>1</v11:InqPageNo><!--固定值-->
				<v11:NoOfPage>10</v11:NoOfPage><!--固定值-->
				<v11:InqType>2</v11:InqType><!--固定值-->
				<v11:InqKeyId>5437906600000194</v11:InqKeyId><!--BankCardSummInq.BankCardRec[0].CardNo-->
			</v11:ServiceBody>
         */

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		envelope.addNamespaceDeclaration("v11", "http://www.obank.com/xsd/TW/DebitCard/DbCardFnctInqRq/v1");
		createSOAPHeader(envelope, date, "DebitCard","DbCardFnctInq");

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement body = soapBody.addChildElement("DbCardFnctInqRq", "v11");
		setSOAPBody(body);

		SOAPElement sb = body.addChildElement("ServiceBody","v11");
		SOAPElement b31 = sb.addChildElement("InqPageNo","v11");
		b31.addTextNode("1");
		SOAPElement b32 = sb.addChildElement("NoOfPage","v11");
		b32.addTextNode("10");	
		SOAPElement b33 = sb.addChildElement("InqType","v11");
		b33.addTextNode("2");	
		SOAPElement b34 = sb.addChildElement("InqKeyId","v11");
		b34.addTextNode(value);	
		soapMessage.saveChanges();
		
		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}
}
