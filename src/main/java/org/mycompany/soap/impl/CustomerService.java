package org.mycompany.soap.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.mycompany.soap.AbstractSaopService;

import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;
import com.obank.xsd.tw.customer.custprofinqrs.v1.ObjectFactory;


public class CustomerService<T> extends AbstractSaopService<CustProfInqRs> {
	
	private String tagName = "CustProfInqRs";
	private final static String endpoint = "https://testsfdcesb.o-bank.com/BusinessServices/ServiceMediationService.serviceagent/ServiceMediationOperation";
	
	
	@Override
	public String getTagName() {
		return tagName;
	}
	@Override
	public String getTestFile() {
		return "C:\\Users\\A60010\\Desktop\\obank\\carddata\\res\\BankCardSummInq_sample_response.xml";
	}
	@Override
	public Class<?> getObjectFactory() {
		return ObjectFactory.class;
	}

	@Override
	public SOAPMessage createSOAPMessage(Date date, String value) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		return soapMessage;
	}
	@Override
	public String getAction() {
		return null;
	}
	public T getTargetTag(InputStream input) throws Throwable {
		return (T) this.getTarget(input);
	}
	
	public CustProfInqRs getCustomerData(Map<String ,String> params) throws Throwable {
		CustProfInqRs custProfInqRs = null;
		SOAPMessage soapMessage = genSoapMessage(params);
		String responseBody = sendHttpRequest(soapMessage);
    	InputStream input = new ByteArrayInputStream(responseBody.getBytes());
    	IOUtils.closeQuietly(input);
        custProfInqRs = (CustProfInqRs) getTargetTag(input);
		
		return custProfInqRs;
	}
	
	/**
	 * Method used to create the SOAP Request
	 */
	private SOAPMessage genSoapMessage(Map<String ,String> params) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
	       
        MimeHeaders headers = soapMessage.getMimeHeaders(); 
        headers.addHeader("Content-Type", "text/soap+xml");
        headers.addHeader("SOAPAction", "/BusinessServices/Customer/CustProfInq/V1/Operation");
        
		SOAPPart soapPart = soapMessage.getSOAPPart();

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		envelope.addNamespaceDeclaration("v11", "http://www.obank.com/xsd/TW/Customer/CustProfInqRq/v1");

		
		createSOAPHeader(envelope, new Date(), "Customer","CustProfInq");
		
//		// SOAP Header
//		SOAPHeader soapHeader = envelope.getHeader();
//		SOAPElement v1Header = soapHeader.addChildElement("Header", "v1");
//		v1Header.setAttribute("v1:mustUnderstand", "1");
//		SOAPElement ChannelID = v1Header.addChildElement("ChannelID");
//		ChannelID.addTextNode("ESB");
//		SOAPElement ServiceDomain = v1Header.addChildElement("ServiceDomain");
//		ServiceDomain.addTextNode("Customer");
//		SOAPElement OperationName = v1Header.addChildElement("OperationName");
//		OperationName.addTextNode("CustProfInq");
//		SOAPElement ConsumerID = v1Header.addChildElement("ConsumerID");
//		ConsumerID.addTextNode("TWESB");
//		SOAPElement TransactionID = v1Header.addChildElement("TransactionID");
//		TransactionID.addTextNode("a1b2c320190718201907181036501231");  //Generate a new Transaction id
//		SOAPElement RqTimestamp = v1Header.addChildElement("RqTimestamp");
//		RqTimestamp.addTextNode("2019-07-18T12:04:53");  //Generate a new request time.
		
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement CustProfInqRq = soapBody.addChildElement("CustProfInqRq", "v11");
		SOAPElement ServiceHeader = CustProfInqRq.addChildElement("ServiceHeader", "v11");
		SOAPElement TxnId = ServiceHeader.addChildElement("TxnId", "v11");
		TxnId.addTextNode("XX001");
		SOAPElement TxnNo = ServiceHeader.addChildElement("TxnNo", "v11");
		TxnNo.addTextNode("T0982323");
		
		SOAPElement Signon = CustProfInqRq.addChildElement("Signon", "v11");
		SOAPElement CustId = Signon.addChildElement("CustId", "v11");
		CustId.addTextNode("IFUTBSF0001");
		SOAPElement AuthToken = Signon.addChildElement("AuthToken", "v11");
		AuthToken.addTextNode("0");

		SOAPElement ServiceBody = CustProfInqRq.addChildElement("ServiceBody", "v11");
		
		//<!--You have a CHOICE of the next 2 items at this level-->
		String customerId = params.get("customerId");
		String CIFNo = params.get("CIFNo");
		
		if(customerId != null && customerId.length() >0) {
			SOAPElement CustPermId = ServiceBody.addChildElement("CustPermId", "v11");
			CustPermId.addTextNode(customerId);  // param by CustPermId
		}else if(CIFNo != null && CIFNo.length() >0) {
			SOAPElement CIFNoElement = ServiceBody.addChildElement("CIFNo", "v11");
			CIFNoElement.addTextNode(CIFNo);  // param by CIFNo
		}
		
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}
	
	public String sendHttpRequest(SOAPMessage soapMessage) {
		String responseBody = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			soapMessage.writeTo(out);
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String strMsg = new String(out.toByteArray());
		System.out.println("***********soap req message ==" + strMsg);
		
		
//		String endpoint = "https://testsfdcesb.o-bank.com/BusinessServices/ServiceMediationService.serviceagent/ServiceMediationOperation";
		
		RequestEntity entity = new StringRequestEntity(strMsg);
		
		
		
		  // HTTP POST 請求物件
		PostMethod post = new PostMethod(endpoint);
		post.setRequestEntity(entity);
		post.setRequestHeader("SOAPAction", "/BusinessServices/Customer/CustProfInq/V1/Operation");
		post.setRequestHeader("Content-type", "application/soap+xml; charset=utf-8");
		post.setRequestHeader("Content-Length", entity.getContentLength() + "");
		HttpClient httpclient = new HttpClient();
		try {
		    int result = 0;
			try {
				result = httpclient.executeMethod(post);
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.out.println("Response status code: " + result);
		    System.out.println("Response body: ");
		    responseBody = post.getResponseBodyAsString();
		    System.out.println(responseBody);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		    post.releaseConnection();
		}
	   return responseBody;
	}
}
