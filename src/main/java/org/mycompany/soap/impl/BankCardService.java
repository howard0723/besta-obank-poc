package org.mycompany.soap.impl;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.mycompany.exception.DataNotFoundException;
import org.mycompany.soap.AbstractSaopService;
import org.springframework.util.CollectionUtils;

import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.BankCardRec;
import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.BankCardSummInqRs;
import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.ObjectFactory;
import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.ServiceBody;
import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.CardBlkRec;
import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.DbCardFnctInqRs;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1.CardLmtRec;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1.DbCardLmtInqRs;


public class BankCardService extends AbstractSaopService<BankCardSummInqRs> {
	
	protected DebitCardImtIngService debitCardImtIng = new DebitCardImtIngService();
	protected DebitCardFnctIngService debitCardFnctIng = new DebitCardFnctIngService();
	private String tagName = "BankCardSummInqRs";
	@Override
	public String getTagName() {
		return tagName;
	}
	@Override
	public String getTestFile() {
		return "C:\\Users\\A60010\\Desktop\\obank\\carddata\\res\\BankCardSummInq_sample_response.xml";
	}
	@Override
	public Class<?> getObjectFactory() {
		return ObjectFactory.class;
	}
	@Override
	public String getAction() {
		return "/BusinessServices/BankCard/BankCardSummInq/V1/Operation";
	}
	@Override
	public SOAPMessage createSOAPMessage(Date date, String value) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

        /*
					<v11:ServiceBody>
						<v11:PageNo>1</v11:PageNo><!--固定值-->
						<v11:NoOfPage>200</v11:NoOfPage><!--固定值-->
						<v11:InqType>2</v11:InqType><!--固定值-->
						<v11:InqKeyId>W121400127</v11:InqKeyId><!--customerId-->
					</v11:ServiceBody>
         */

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		envelope.addNamespaceDeclaration("v11", "http://www.obank.com/xsd/TW/BankCard/BankCardSummInqRq/v1");
		createSOAPHeader(envelope, date, "BankCard","BankCardSummInq");
		
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement body = soapBody.addChildElement("BankCardSummInqRq", "v11");
		setSOAPBody(body);
		
		SOAPElement sb = body.addChildElement("ServiceBody","v11");
		SOAPElement b31 = sb.addChildElement("PageNo","v11");
		b31.addTextNode("1");
		SOAPElement b32 = sb.addChildElement("NoOfPage","v11");
		b32.addTextNode("200");	
		SOAPElement b33 = sb.addChildElement("InqType","v11");
		b33.addTextNode("2");	
		SOAPElement b34 = sb.addChildElement("InqKeyId","v11");
		b34.addTextNode(value);	
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}
	
	public Map<String,Object> getCards(String customerId, Date date) throws Throwable {
		BankCardSummInqRs summ = this.execute(new Date(), customerId);
		ServiceBody sb = summ.getServiceBody();
		if ("98".equals(sb.getRtrnCode())) {
			throw new DataNotFoundException();
		} else if ("00".equals(sb.getRtrnCode()) == false) {
			throw new IllegalStateException(sb.getRtrnCode());
		}
		
		BankCardRec bcr = sb.getBankCardRec().get(0);
		String cardNo = bcr.getCardNo();
		
		
		DbCardLmtInqRs dblr = debitCardImtIng.execute(date, cardNo);
		if (!("92".equals(dblr.getServiceBody().getRtrnCode()) || "00".equals(dblr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(sb.getRtrnCode());
		}
		
		DbCardFnctInqRs dbfr = debitCardFnctIng.execute(date, cardNo);
		if (!("91".equals(dbfr.getServiceBody().getRtrnCode()) || "00".equals(dbfr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(sb.getRtrnCode());
		}
		
		CardLmtRec clr = null;
		if (CollectionUtils.isEmpty(dblr.getServiceBody().getCardLmtRec()) == false) {
			clr = dblr.getServiceBody().getCardLmtRec().get(0);
		}
		
		CardBlkRec cbr = null;
		if (CollectionUtils.isEmpty(dbfr.getServiceBody().getCardBlkRec()) == false) {
			cbr = dbfr.getServiceBody().getCardBlkRec().get(0);
		}
		
		Map<String,Object> cards = new LinkedHashMap<String,Object>();
		
		cards.put("cardNo", cardNo);
		cards.put("cardType", bcr.getCardFaceName());
		cards.put("cardStatus", bcr.getCardIssueStat());
		
		if (clr != null) {
			cards.put("dailyLimitAmount", clr.getAvailDayLmt());
			cards.put("monthlyLimitAmount", clr.getAvailMnthLmt());
			cards.put("usedDailyAmount", clr.getAccumDayLmt());
			cards.put("usedMonthlyAmount", clr.getAccumMnthLmt());
		} else {
			cards.put("dailyLimitAmount", "");
			cards.put("monthlyLimitAmount", "");
			cards.put("usedDailyAmount", "");
			cards.put("usedMonthlyAmount", "");
		}
		
		if (cbr != null) {
			cards.put("expireMonthYear", cbr.getExpYearMnth());
		} else {
			cards.put("expireMonthYear", "");
		}
		return cards;
	}
}
