package org.mycompany.soap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public abstract class AbstractSaopService<T> {
	protected static SimpleDateFormat SDF1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	protected static SimpleDateFormat SDF2 = new SimpleDateFormat("yyyyMMdd");
	protected static String CLIENT_ID = "a1b2c3d4e5f6e7d8c9b0a1";
	private Logger logger = LoggerFactory.getLogger(getClass());
	abstract public SOAPMessage createSOAPMessage(Date date, String value) throws Exception;
	abstract public String getTagName(); 
	abstract public String getTestFile(); 
	abstract public Class<?> getObjectFactory();
	abstract public String getAction();
	protected String getTxid(Date date) {
		String rtn = StringUtils.left(CLIENT_ID, 6);
		rtn = rtn + SDF2.format(date);
		
		try {
			HttpServletRequest request = 
			        ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes())
			                .getRequest();
			rtn = rtn + (StringUtils.isNotBlank(request.getHeader("uuid")) ? request.getHeader("uuid") : "") ;
		} catch (Throwable e) {
			logger.warn(e.getMessage());
		}
		return rtn;
	}
	
	protected SOAPHeader createSOAPHeader(SOAPEnvelope envelope, Date date, String serviceDomain, String opnName) throws Exception {
		SOAPHeader header = envelope.getHeader();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		SOAPElement h1 = header.addChildElement("Header","v1");
		h1.setAttribute("v1:mustUnderstand", "1");
		SOAPElement h11 = h1.addChildElement("ChannelID");
		h11.addTextNode("ESB");
		SOAPElement h12 = h1.addChildElement("ServiceDomain");
		h12.addTextNode(serviceDomain);
		SOAPElement h13 = h1.addChildElement("OperationName");
		h13.addTextNode(opnName);
		SOAPElement h14 = h1.addChildElement("ConsumerID");
		h14.addTextNode("TWESB");
		SOAPElement h15 = h1.addChildElement("TransactionID");
		h15.addTextNode(getTxid(date));
		SOAPElement h16 = h1.addChildElement("RqTimestamp");
		h16.addTextNode(SDF1.format(date));
		return header;
	}
	
	protected void setSOAPBody(SOAPElement body) throws Exception {
		SOAPElement b1 = body.addChildElement("ServiceHeader","v11");
		SOAPElement b11 = b1.addChildElement("TxnId","v11");
		b11.addTextNode("XX001");
		SOAPElement b12 = b1.addChildElement("TxnNo","v11");
		b12.addTextNode("T0982323");		
		
		SOAPElement b2 = body.addChildElement("Signon","v11");
		SOAPElement b21 = b2.addChildElement("CustId","v11");
		b21.addTextNode("IFUTBSF0001");
		SOAPElement b22 = b2.addChildElement("AuthToken","v11");
		b22.addTextNode("0");
	}

	
	/**
	 * Method used to print the SOAP Response
	 */
	private T getResponse(SOAPMessage soapResponse) throws Throwable {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(out);
		transformer.transform(sourceContent, result);
		
		logger.info("Response SOAP Message");
		logger.info(StringUtils.toEncodedString(out.toByteArray(), Charset.forName("UTF-8")));
		InputStream input = new ByteArrayInputStream(out.toByteArray());
		T rtn = getTarget(input);
		IOUtils.closeQuietly(input);
		return rtn;
	}
	
	public T execute2(Date date, String value) throws Throwable {
		SOAPConnection soapConnection = null;
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			String endpoint = "https://testsfdcesb.o-bank.com/BusinessServices/ServiceMediationService.serviceagent/ServiceMediationOperation";
			SOAPMessage soapResponse = soapConnection.call(createSOAPMessage(date, value), endpoint);

			// Process the SOAP Response
			return getResponse(soapResponse);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new IllegalStateException(e);
		} finally {
			try {
				soapConnection.close();
			} catch (Throwable e) {
				logger.error(e.getMessage(),e);
			}
		}
	}
	
	public T execute(Date date, String value) throws Throwable {
		String endpoint = "https://testsfdcesb.o-bank.com/BusinessServices/ServiceMediationService.serviceagent/ServiceMediationOperation";
		PostMethod post = new PostMethod(endpoint);
		try {
			
			SOAPMessage message = createSOAPMessage(date, value);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			message.writeTo(out);
			String strMsg = new String(out.toByteArray());
			
			  // HTTP POST 請求物件
			RequestEntity entity = new StringRequestEntity(strMsg);

			post.setRequestEntity(entity);
			post.setRequestHeader("SOAPAction", getAction());
			post.setRequestHeader("Content-type", "application/soap+xml; charset=utf-8");
			post.setRequestHeader("Content-Length", entity.getContentLength() + "");
			HttpClient httpclient = new HttpClient();
		    int result = httpclient.executeMethod(post);
			logger.info("HttpCode="+result);
			logger.info("Response Message");
			logger.info(post.getResponseBodyAsString());
		    InputStream input = new ByteArrayInputStream(post.getResponseBody());
		    return getTarget(input);
		} catch (Throwable e) {
			logger.error(e.getMessage(),e);
			throw new IllegalStateException(e);
		} finally {
			try {
				post.releaseConnection();
			} catch (Throwable e) {
				logger.error(e.getMessage(),e);
			}
		}
	}
	
	public T test(Date date, String value) throws Throwable {
    	FileInputStream input = new FileInputStream(getTestFile());
		T rtn = getTarget(input);
		IOUtils.closeQuietly(input);
		return rtn;
	}
	
	@SuppressWarnings("unchecked")
	protected T getTarget(InputStream input) throws Throwable {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        StreamSource xml = new StreamSource(input);
        XMLStreamReader xmlStreamReader = xif.createXMLStreamReader(xml);
        int event = xmlStreamReader.getEventType();
        //
        while(xmlStreamReader.hasNext()){
        	if (event == XMLStreamConstants.START_ELEMENT && xmlStreamReader.getLocalName().equals(getTagName())) {
        		logger.debug(xmlStreamReader.getLocalName());
        		break;
        	}
            event = xmlStreamReader.next();
        }
        
        
    	JAXBContext context = JAXBContext.newInstance(getObjectFactory());
    	Unmarshaller unmarshaller = context.createUnmarshaller();
    	
    	JAXBElement<T> o = (JAXBElement<T>) unmarshaller.unmarshal(xmlStreamReader);
    	return o.getValue();
	}
}
