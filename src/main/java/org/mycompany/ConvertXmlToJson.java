package org.mycompany;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.mycompany.soap.impl.CustomerService;

import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInfo;
import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;

public class ConvertXmlToJson {

	public static void main(String[] args) {

		String data = "<ns0:Envelope xmlns:ns0=\"http://www.w3.org/2003/05/soap-envelope\">\r\n" + 
			  		"	<ns0:Header>\r\n" + 
			  		"		<ns:Header ns:mustUnderstand=\"true\" xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ns=\"http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1\">\r\n" + 
			  		"			<ChannelID>ESB</ChannelID>\r\n" + 
			  		"			<ServiceDomain>Customer</ServiceDomain>\r\n" + 
			  		"			<OperationName>CustProfInq</OperationName>\r\n" + 
			  		"			<ConsumerID>TWESB</ConsumerID>\r\n" + 
			  		"			<SourceRegion/>\r\n" + 
			  		"			<DestinationRegion/>\r\n" + 
			  		"			<TransactionID>WilliamTest001-1</TransactionID>\r\n" + 
			  		"			<TrackingID>7ce71f60-3c0e-46c0-ac7d-da9c81f58367</TrackingID>\r\n" + 
			  		"			<UUID>3f8ebccf-3268-4fee-b937-64c5d6026406WwNUCQ0UDBRBVw==</UUID>\r\n" + 
			  		"			<CorrelationID/>\r\n" + 
			  		"			<RqTimestamp>2019-07-17T10:00:02.009+08:00</RqTimestamp>\r\n" + 
			  		"			<RsTimestamp>2019-07-17T10:00:33.106+08:00</RsTimestamp>\r\n" + 
			  		"		</ns:Header>\r\n" + 
			  		"	</ns0:Header>\r\n" + 
			  		"	<ns0:Body>\r\n" + 
			  		"		<ns0:CustProfInqRs xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ns0=\"http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1\">\r\n" + 
			  		"			<ns0:ServiceHeader>\r\n" + 
			  		"				<ns0:TxnId>XX001</ns0:TxnId>\r\n" + 
			  		"				<ns0:TxnNo>T0982323</ns0:TxnNo>\r\n" + 
			  		"			</ns0:ServiceHeader>\r\n" + 
			  		"			<ns0:Signon>\r\n" + 
			  		"				<ns0:CustId>IFUTBSF0001</ns0:CustId>\r\n" + 
			  		"				<ns0:AuthToken>0</ns0:AuthToken>\r\n" + 
			  		"			</ns0:Signon>\r\n" + 
			  		"			<ns0:ServiceBody>\r\n" + 
			  		"				<ns0:RtrnStatCode>\r\n" + 
			  		"					<ns0:RtrnCode>00000</ns0:RtrnCode>\r\n" + 
			  		"				</ns0:RtrnStatCode>\r\n" + 
			  		"				<ns0:CustProfInfo>\r\n" + 
			  		"					<ns0:CIFNo>1000004844</ns0:CIFNo>\r\n" + 
			  		"					<ns0:Mnem>W121400127</ns0:Mnem>\r\n" + 
			  		"					<ns0:CustShrtName>NAME.1-1000004844</ns0:CustShrtName>\r\n" + 
			  		"					<ns0:CustShrtName>NAME.2-1000004844</ns0:CustShrtName>\r\n" + 
			  		"					<ns0:CustEngName>NAME.1-1000004844</ns0:CustEngName>\r\n" + 
			  		"					<ns0:CustChnName>NAME.2-1000004844</ns0:CustChnName>\r\n" + 
			  		"					<ns0:MailAddr>\r\n" + 
			  		"						<ns0:St>\r\n" + 
			  		"							<ns0:GBSt>Yanngruang St, 114</ns0:GBSt>\r\n" + 
			  		"							<ns0:TWSt>Yanngruang St, 114</ns0:TWSt>\r\n" + 
			  		"						</ns0:St>\r\n" + 
			  		"						<ns0:TownCnty>\r\n" + 
			  		"							<ns0:GBTownCnty>Neihu Dist, Taipei</ns0:GBTownCnty>\r\n" + 
			  		"							<ns0:TWTownCnty>Neihu Dist, Taipei</ns0:TWTownCnty>\r\n" + 
			  		"						</ns0:TownCnty>\r\n" + 
			  		"						<ns0:AddrZip>\r\n" + 
			  		"							<ns0:GBAddrZip>242</ns0:GBAddrZip>\r\n" + 
			  		"							<ns0:TWAddrZip>242</ns0:TWAddrZip>\r\n" + 
			  		"						</ns0:AddrZip>\r\n" + 
			  		"						<ns0:Cntry>TW</ns0:Cntry>\r\n" + 
			  		"					</ns0:MailAddr>\r\n" + 
			  		"					<ns0:BirthAddr>\r\n" + 
			  		"						<ns0:Cntry/>\r\n" + 
			  		"						<ns0:City/>\r\n" + 
			  		"					</ns0:BirthAddr>\r\n" + 
			  		"					<ns0:Sctr>1001</ns0:Sctr>\r\n" + 
			  		"					<ns0:AOEmplNo>1</ns0:AOEmplNo>\r\n" + 
			  		"					<ns0:Indstry>1</ns0:Indstry>\r\n" + 
			  		"					<ns0:Tgt>1</ns0:Tgt>\r\n" + 
			  		"					<ns0:Natl>TW</ns0:Natl>\r\n" + 
			  		"					<ns0:CustStat>1</ns0:CustStat>\r\n" + 
			  		"					<ns0:Res>TW</ns0:Res>\r\n" + 
			  		"					<ns0:CertLic>\r\n" + 
			  		"						<ns0:CertLicName>ID.CARD</ns0:CertLicName>\r\n" + 
			  		"						<ns0:CertLicIssDate>2016-12-15</ns0:CertLicIssDate>\r\n" + 
			  		"					</ns0:CertLic>\r\n" + 
			  		"					<ns0:OffPhnNo>0287527000</ns0:OffPhnNo>\r\n" + 
			  		"					<ns0:Lang>1</ns0:Lang>\r\n" + 
			  		"					<ns0:Gndr>MALE</ns0:Gndr>\r\n" + 
			  		"					<ns0:BirthDay>1990-07-12</ns0:BirthDay>\r\n" + 
			  		"					<ns0:PhnNo1>81783177</ns0:PhnNo1>\r\n" + 
			  		"					<ns0:MobNo1>0936096024</ns0:MobNo1>\r\n" + 
			  		"					<ns0:EMailAddr1>scramble@o-bank.com</ns0:EMailAddr1>\r\n" + 
			  		"					<ns0:Empl>\r\n" + 
			  		"						<ns0:Slry>900012.0</ns0:Slry>\r\n" + 
			  		"					</ns0:Empl>\r\n" + 
			  		"					<ns0:ResStat/>\r\n" + 
			  		"					<ns0:CIFAddDate>2016-12-14</ns0:CIFAddDate>\r\n" + 
			  		"					<ns0:EffDate>2016-12-14</ns0:EffDate>\r\n" + 
			  		"					<ns0:CurRecNo>3</ns0:CurRecNo>\r\n" + 
			  		"					<ns0:EntryEmplNo>96775_IFUCC.I0001__WS___OFS_IFPA</ns0:EntryEmplNo>\r\n" + 
			  		"					<ns0:UpdtDate>1907111412</ns0:UpdtDate>\r\n" + 
			  		"					<ns0:AuthEmplId>96775_IFUCC.I0001_WS____OFS_IFPA</ns0:AuthEmplId>\r\n" + 
			  		"					<ns0:BrchId>TW0019999</ns0:BrchId>\r\n" + 
			  		"					<ns0:DeptId>1</ns0:DeptId>\r\n" + 
			  		"					<ns0:LPIDNo>W121400127</ns0:LPIDNo>\r\n" + 
			  		"					<ns0:LPIPAFlg>Y</ns0:LPIPAFlg>\r\n" + 
			  		"					<ns0:LFXId>3</ns0:LFXId>\r\n" + 
			  		"					<ns0:LPIDIssDate>2016-12-15</ns0:LPIDIssDate>\r\n" + 
			  		"					<ns0:LIndstryCode>24</ns0:LIndstryCode>\r\n" + 
			  		"					<ns0:LIndstryObj>060000</ns0:LIndstryObj>\r\n" + 
			  		"					<ns0:LNHIFlg>Y</ns0:LNHIFlg>\r\n" + 
			  		"					<ns0:LRes183Flg>Y</ns0:LRes183Flg>\r\n" + 
			  		"					<ns0:LCtctPhnExt>9000</ns0:LCtctPhnExt>\r\n" + 
			  		"					<ns0:LWMRMCode>1568</ns0:LWMRMCode>\r\n" + 
			  		"					<ns0:LCustType>R</ns0:LCustType>\r\n" + 
			  		"					<ns0:LPayRollCode>B24339474</ns0:LPayRollCode>\r\n" + 
			  		"					<ns0:LAMLFlg>E</ns0:LAMLFlg>\r\n" + 
			  		"					<ns0:LChanCode>E</ns0:LChanCode>\r\n" + 
			  		"					<ns0:LCollBrchId>TW0010001</ns0:LCollBrchId>\r\n" + 
			  		"					<ns0:LInpBrchId>TW0010001</ns0:LInpBrchId>\r\n" + 
			  		"					<ns0:LTxnNo>OBT187563097653411.10</ns0:LTxnNo>\r\n" + 
			  		"					<ns0:LVerName>CUSTOMER,OBT.INTF.INDIV.AMD</ns0:LVerName>\r\n" + 
			  		"					<ns0:LBussType>R</ns0:LBussType>\r\n" + 
			  		"					<ns0:InpBrchId>TW0010001</ns0:InpBrchId>\r\n" + 
			  		"					<ns0:SigFlg>1</ns0:SigFlg>\r\n" + 
			  		"					<ns0:AddrRtrnStatCode>\r\n" + 
			  		"						<ns0:RtrnCode>00000</ns0:RtrnCode>\r\n" + 
			  		"					</ns0:AddrRtrnStatCode>\r\n" + 
			  		"					<ns0:RsdntAddr>\r\n" + 
			  		"						<ns0:RsdntAddrId>TW0019999.C-1000004844.PRINT.2</ns0:RsdntAddrId>\r\n" + 
			  		"						<ns0:CustEngName>NAME.1-1000004844</ns0:CustEngName>\r\n" + 
			  		"						<ns0:CustChnName>NAME.2-1000004844</ns0:CustChnName>\r\n" + 
			  		"						<ns0:St>\r\n" + 
			  		"							<ns0:GBSt>Yanngruang St, 114</ns0:GBSt>\r\n" + 
			  		"							<ns0:TWSt>Yanngruang St, 114</ns0:TWSt>\r\n" + 
			  		"						</ns0:St>\r\n" + 
			  		"						<ns0:TownCnty>\r\n" + 
			  		"							<ns0:GBTownCnty>Neihu Dist, Taipei</ns0:GBTownCnty>\r\n" + 
			  		"							<ns0:TWTownCnty>Neihu Dist, Taipei</ns0:TWTownCnty>\r\n" + 
			  		"						</ns0:TownCnty>\r\n" + 
			  		"						<ns0:AddrZip>\r\n" + 
			  		"							<ns0:GBAddrZip>242</ns0:GBAddrZip>\r\n" + 
			  		"							<ns0:TWAddrZip>242</ns0:TWAddrZip>\r\n" + 
			  		"						</ns0:AddrZip>\r\n" + 
			  		"						<ns0:Cntry>TW</ns0:Cntry>\r\n" + 
			  		"						<ns0:PhnNo1>81783177</ns0:PhnNo1>\r\n" + 
			  		"					</ns0:RsdntAddr>\r\n" + 
			  		"				</ns0:CustProfInfo>\r\n" + 
			  		"			</ns0:ServiceBody>\r\n" + 
			  		"		</ns0:CustProfInqRs>\r\n" + 
			  		"	</ns0:Body>\r\n" + 
			  		"</ns0:Envelope>";
           
		CustomerService cs = new CustomerService();
	    try {
	    	InputStream input = new ByteArrayInputStream(data.getBytes());
	    	String tagName = "";
	    	CustProfInqRs custProfInqRs = (CustProfInqRs) cs.getTargetTag(input);
			IOUtils.closeQuietly(input);
			System.out.println(custProfInqRs.getServiceBody().getCustProfInfo().getBrchId());
			genJSon(custProfInqRs);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
    }
	
	public static void genJSon(CustProfInqRs custProfInqRs) {
		JSONObject json = new JSONObject();
		CustProfInfo custProfInfo = custProfInqRs.getServiceBody().getCustProfInfo();
		json.put("CIFNo", custProfInfo.getCIFNo());
		json.put("customerId", custProfInfo.getMnem());
		json.put("chineseName", custProfInfo.getCustChnName());
		json.put("nickName", custProfInfo.getCustShrtName());
		json.put("birthday", custProfInfo.getBirthDay());
		json.put("cellphoneNo", custProfInfo.getMobNo1());
		json.put("email", custProfInfo.getEMailAddr1());
		JSONObject addressNodeJson = new JSONObject();
		JSONObject streetNodeJson = new JSONObject();
		json.put("address", addressNodeJson);
		addressNodeJson.put("street", streetNodeJson);
		streetNodeJson.put("global", custProfInfo.getMailAddr().getSt().getGBSt());
		streetNodeJson.put("taiwan", custProfInfo.getMailAddr().getSt().getTWSt());
		
		JSONObject townNodeJson = new JSONObject();
		townNodeJson.put("global", custProfInfo.getMailAddr().getTownCnty().getGBTownCnty());
		townNodeJson.put("taiwan", custProfInfo.getMailAddr().getTownCnty().getTWTownCnty());
		
		json.put("townCountry",townNodeJson);
		
		JSONObject zipNodeJson = new JSONObject();
		zipNodeJson.put("global", custProfInfo.getMailAddr().getAddrZip().getGBAddrZip());
		zipNodeJson.put("taiwan", custProfInfo.getMailAddr().getAddrZip().getTWAddrZip());
		
		json.put("zip", zipNodeJson);
		json.put("country", custProfInfo.getMnem());
		System.out.println(json);
	}

}
